<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

get_header(); ?>

	<!-- <div id="primary" class="content-area"> -->
		<?php get_sidebar('topinfo') ?>
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) :
			if ( is_author() ) {
				$author = get_queried_object();
				$author_id = $author->ID;

				$user_foto = get_field('user_foto', 'user_'.$author_id);
				$size = 'full'; // (potřebujeme plnou velikost 400px ať to dobře vypadá na retině)
			}
			?>
			<header class="page-header<?php if( is_author() && $user_foto ) { echo ' has-user-photo'; } ?>">
				<?php
					if ( is_author() && $user_foto ) {
						echo wp_get_attachment_image( $user_foto['ID'], $size );
						echo '<div class="user-container">';
					}

					the_archive_title( '<h1 class="archive-title">', '</h1>' );
					if ( is_author() ) {
						echo '<span class="author-email"><i class="icon share-email"></i> <a href="mailto:'.get_the_author_meta( 'email' ).'" title="e-mail autora">Napsat autorovi</a></span>';

						if ( $user_foto ) {
							echo '</div><!-- !user-container -->';
						}
					}
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
				if ( is_author() ) {
					echo '<h2 class="author-posts-title">Články autora</h2>';
				}
			?>

			<?php
			/* Start the Loop */
			$counter = 0;
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				if ( ($counter == 0) && ( has_post_thumbnail() ) && !is_author() ) {
					get_template_part( 'template-parts/main-post' );
					$counter++;
				} else {
					get_template_part( 'template-parts/content', get_post_format() );
				}

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	<!-- </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
