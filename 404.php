<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package eHutnik_1.0
 */

get_header(); ?>

    <!--<div id="primary" class="content-area">-->
        <main id="main" class="site-main">

            <section class="error-404 not-found">
                <header class="page-header">
                    <h1 class="page-title">404</h1>
                    <p class="description404">Požadovanou stránku jsme bohužel nenašli</p>
                </header><!-- .page-header -->

                <div class="page-content">
                    <p>Své želízko v ohni můžete zkusit vyhledat nebo mrkněte do <a href="http://archiv.ehutnik.cz">archivu zpráv do r. 2017</a>.
                    <br><small>Tip: Je-li zpráva z archivu, zkuste v původní adrese nahradit <strong>www.ehutnik.cz</strong> za <strong>archiv.ehutnik.cz,</strong> mohli byste mít štěstí.</small></p>


                    <?php
                        get_search_form();
                    ?>
                </div><!-- .page-content -->
            </section><!-- .error-404 -->

        </main><!-- #main -->
    <!--</div><!-- #primary -->

<?php
get_footer();
