<?php
/**
 * Template Name: Kontakty
 *
 * This is the template that formats page with contacts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

get_header(); ?>

	<!--<div id="primary" class="content-area">-->
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.

			// Printing contacts
			// Check if the repeater field has rows of data
			if( have_rows('kontakty-sekce') ):

				echo '<div class="kontakty">';
				// loop through the rows of data
				while ( have_rows('kontakty-sekce') ) : the_row();

					// display setion of contacts
					echo '<div class="kontakty-row">';
					if (get_sub_field('kontakty_sekce_titulek')) {
						echo '<h2 class="widget-title">'.get_sub_field('kontakty_sekce_titulek').'</h2>';
					}
					echo '<div class="kontakty-container">';

					remove_filter('acf_the_content', 'wpautop' );
					echo get_sub_field('kontakty_sekce_obsah');
					add_filter('acf_the_content', 'wpautop' , 12);
					echo '</div>';
					echo '</div>';

				endwhile;
				echo '</div>';

			else :

				// no rows found

			endif;

			?>

		</main><!-- #main -->
	<!--</div><!-- #primary -->

<?php
get_footer();
