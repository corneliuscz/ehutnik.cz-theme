<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eHutnik_1.0
 */

if ( ! is_active_sidebar( 'sidebar-topinfo' ) ) {
	return;
}
?>

<aside id="topinfo" class="widget-area">
	<?php dynamic_sidebar( 'sidebar-topinfo' ); ?>
</aside> <!-- #topinfo -->
