<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eHutnik_1.0
 */

if ( ! is_active_sidebar( 'sidebar-toplinks' ) ) {
	return;
}
?>

<aside id="toplinks" class="links-area">
	<?php dynamic_sidebar( 'sidebar-toplinks' ); ?>
</aside> <!-- #topinfo -->
