<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

?>

<article id="gallery-<?php the_ID(); ?>" <?php post_class( 'post--gallery' ); ?>>
	<div class="wrapper">
		<div class="gallery-data">
		<?php
			$images = get_field('galerie');
			echo( count($images) );
		?>
		</div>
		<div class="header-image">
		<?php if ( !is_singular() ) : ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
		<?php endif; ?>
		<?php
			if ( has_post_thumbnail() ) {
				the_post_thumbnail( true , array('class' => 'obrazem-thumb') );
			}
		?>

		<?php if ( !is_singular() ) : ?>
			</a>
		<?php endif; ?>
		</div>
	</div>

    <header class="entry-header">
        <?php

        if ( is_singular() ) :
            the_title( '<h1 class="entry-title">', '</h1>' );
        else :
            the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
        endif;
        ?>
    </header><!-- .entry-header -->

</article><!-- #gallery-<?php the_ID(); ?> -->
