<?php
/**
 * Template part for displaying Status format posts (Short news) in search results and archives
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( is_single($post->ID) ? '' : 'post--regular' ); ?>>
    <header class="entry-header">
        <?php
        if ( 'post' === get_post_type() ) : ?>
        <div class="entry-meta">
            <?php ehutnik_entry_meta(); ?>
        </div><!-- .entry-meta -->
        <?php
        endif;

        if ( is_singular() ) {
            the_title( '<h1 class="entry-title screen-reader-text">', '</h1>' );
		} else {
            //the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
        }
        ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php
		if ( !is_singular() ) { echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark" class="read-more">'; }
			echo wp_kses(
				wp_trim_words( get_the_content(), 30, '…'),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			);
			/*
			sprintf(
                wp_kses(
                    // translators: %s: Name of current post. Only visible to screen readers
                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'ehutnik' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            )
			*/
		if ( !is_singular() ) { echo '</a>'; }

            wp_link_pages( array(
                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'ehutnik' ),
                'after'  => '</div>',
            ) );
        ?>
    </div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
