<?php
/**
 * Template part for displaying posts
 * Category posts on homepage with thumbnails
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

    $thumbnail = has_post_thumbnail();
    if (!$thumbnail) { $thumb_class = 'no-post-thumbnail'; }
    else { $thumb_class = ''; }

    if ($first_article) { $first_class = 'post--featured'; }
    else { $first_class = 'post--nointro'; }

    $classes = array( $thumb_class, $first_class );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
    <div class="featured-wrapper">
        <a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark" class="header-image">
        <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail();
        } else {
            echo '<img src="'.get_template_directory_uri().'/assets/img/no-article-photo.png" alt="Článek nemá titulní fotografii">';
        }
        ?>
        </a>

        <header class="entry-header">
            <?php
            if ( 'post' === get_post_type() ) : ?>
                <div class="entry-meta">
                    <?php ehutnik_entry_meta(); ?>
                </div><!-- .entry-meta -->
            <?php
            endif;

            the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
            ?>
        </header><!-- .entry-header -->
    </div><!-- .featured-wrapper -->
</article><!-- #post-<?php the_ID(); ?> -->
