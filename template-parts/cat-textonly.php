<?php
/**
 * Template part for displaying posts
 * Version: Text Only posts of category on Homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'post--textonly' ); ?>>
	<header class="entry-header">
		<?php
		the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );

		if ( 'post' === get_post_type() ) : ?>
			<div class="entry-meta">
				<?php ehutnik_entry_meta(); ?>
			</div><!-- .entry-meta -->
		<?php
		endif;

		?>
	</header><!-- .entry-header -->
	<div class="entry-content">
        <?php
		if ( !is_singular() ) { echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark" class="read-more">'; }
            the_content( sprintf(
                wp_kses(
                    /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'ehutnik' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            ) );
		if ( !is_singular() ) { echo '</a>'; }
        ?>
    </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
