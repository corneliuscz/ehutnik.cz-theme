<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'post--featured' ); ?>>
	<div class="featured-wrapper">
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="header-image">
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
			<?php
				the_post_thumbnail();
			?>
			</a>
		</div>
	<?php } ?>
	<div class="entry-data">
    	<header class="entry-header">
    	<?php
			the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );

        	if ( 'post' === get_post_type() ) : ?>
        	<div class="entry-meta">
        	<?php
            	if ( is_sticky( $post->ID ) ) {
                	echo '<span class="meta-important">'.esc_html__( 'Zpráva dne', 'ehutnik' ).'</span> ';
            	}

            	ehutnik_entry_meta();
        	?>
        	</div><!-- .entry-meta -->
    	<?php
        	endif;
    	?>
    	</header><!-- .entry-header -->

    	<div class="entry-intro">
    	<?php
            $content = get_extended( $post->post_content );
            echo apply_filters( 'the_content', $content['main'] );
    	?>
    	</div><!-- .entry-content -->
	</div><!-- .entry-data -->
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
