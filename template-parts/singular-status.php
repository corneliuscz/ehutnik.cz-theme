<?php
/**
 * Template part for displaying STATUS posts in Singular form
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( is_single($post->ID) ? '' : 'post--regular' ); ?>>

    <header class="entry-header">
        <?php
        	the_title( '<h1 class="entry-title">', '</h1>' );

			if ( 'post' === get_post_type() ): ?>
			<div class="entry-meta-row">
	        	<div class="entry-meta">
	            	<?php ehutnik_entry_meta(); ?>
	        	</div><!-- .entry-meta -->
			</div><!-- .entry-meta-row -->
	        <?php
	        endif;
	    ?>
    </header><!-- .entry-header -->

	<div class="entry-content format-status">
		<?php the_content(); ?>
	</div>

	<footer class="entry-footer">
		<div class="share-this left">
			<?php ehutnik_share_this() ?>
		</div><!-- .share-this -->
		<?php ehutnik_posted_by(); ?>
		<!-- .author-link -->
	</footer>

</article><!-- #post-<?php the_ID(); ?> -->
