<?php
/**
 * Template part for displaying SINGULAR posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

?>

<?php
$article = get_extended( $post->post_content );
$images = get_field('galerie');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( is_single($post->ID) ? '' : 'post--regular' ); ?>>

    <header class="entry-header">
        <?php
            the_title( '<h1 class="entry-title">', '</h1>' );

            echo apply_filters('the_content', $article['main']);

        if ( 'post' === get_post_type() || 'obrazem' === get_post_type() ) : ?>
        <div class="entry-meta-row">
            <div class="entry-meta">
                <?php ehutnik_entry_meta(); ?>
            </div><!-- .entry-meta -->
            <div class="share-this right">
                <?php ehutnik_share_this() ?>
            </div><!-- .share-this -->
        </div><!-- .entry-meta-row -->
        <?php
        endif;
        ?>

        <?php if ( has_post_thumbnail() ) { ?>
        <div class="header-image">
            <figure>
                <div class="wrapper">

                <?php

//					$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );
//					if ( ! empty( $large_image_url[0] ) ) {
//						printf( '<a class="lightbox" href="%1$s" alt="%2$s">%3$s</a>',
//							esc_url( $large_image_url[0] ),
//							esc_attr( get_the_title_attribute( 'echo=0' ) ),
//							get_the_post_thumbnail()
//						);
//					}
					if ( !$images ) echo '<a id="postImage-single">';
                    the_post_thumbnail();
                	if ( !$images ) echo '</a>';

                    if ( $images ) {
                        echo '<a id="postGallery" class="gallery-data" title="Otevřít fotogalerii článku">';
                        echo '<span>'. count($images) .' <span class="screen-reader-text">fotografií v galerii</span></span>';
                        echo '</a>';
                    }
                ?>

                </div>

                <?php if ( $caption = get_post( get_post_thumbnail_id() )->post_content ) : ?>
                <figcaption class="figcaption"><?php echo $caption; ?></figcaption>
                <?php endif; ?>

            </figure>
        </div>
        <?php }    // if has_post_thumbnail() ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php
        echo apply_filters('the_content', $article['extended']);
        ?>
    </div><!-- .entry-content -->

    <footer class="entry-footer">
        <div class="share-this left">
            <?php ehutnik_share_this() ?>
        </div><!-- .share-this -->
        <?php
/*
            // Echo "Inzerce" instead of author info if the post is an AD.
            $inzertni = FALSE;
            // Do not echo "Dobré rady"
            $dobrarada = FALSE;
            $terms = get_the_terms($post->ID, 'category');

            foreach ($terms as $term) {
                if ($term->term_id == 12) {
                    $inzertni = TRUE;
                } elseif ($term->term_id == 4) {
                    $dobrarada = TRUE;
                }
            }

            if ( $inzertni ) {
                echo '<span class="ehutnik-adlabel">Inzerce</span>';
            } elseif ( $dobrarada ) {
                echo '';
            } else {
                ehutnik_posted_by();
            }
*/
        ehutnik_posted_by();
        ?>
        <!-- .author-link -->
    </footer>

    <?php
    if ( $images ) {
        // Get data for lightbox/gallery
        echo ehutnik_lightbox( $images );
    } // if count(images)

    echo ehutnik_lightbox_single();

    ?>
</article><!-- #post-<?php the_ID(); ?> -->
