<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eHutnik_1.0
 */

?>

    </div><!-- #content -->

<?php if ( is_home() && is_front_page() ) : ?>
    <!-- homepage blocks -->
    <?php
        get_sidebar( 'hpcats' );
    ?>
    <!-- homepage blocks -->
<?php endif ?>
<?php if ( is_single() || is_archive() ) : ?>
    <!-- Single post blocks -->
    <?php
        get_sidebar('foobar');
    ?>
    <!-- Single post blocks -->
<?php endif ?>

    <footer id="colophon" class="site-footer">
        <div class="footer-nav">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="logo icon ehutnik_negativ"><span class="screen-reader-text">eHutník</span></a>

            <nav id="footer-navigation" class="footer-navigation">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-footer',
                        'menu_id'        => 'footer-menu',
                    ) );
                ?>
            </nav><!-- #site-navigation -->
        </div>
        <div class="site-info">

            <div class="footer-socials">
                <?php if ( !empty(get_field('ss_facebook', 'option'))) : ?>
                    <a href="<?php esc_url( the_field('ss_facebook', 'option')); ?>" class="icon ss-facebook"><span class="screen-reader-text">Facebook</span></a>
                <?php endif; ?>
                <?php if ( !empty(get_field('ss_twitter', 'option'))) : ?>
                    <a href="<?php esc_url( the_field('ss_twitter', 'option')); ?>" class="icon ss-twitter"><span class="screen-reader-text">Twitter</span></a>
                <?php endif; ?>
                <?php if ( !empty(get_field('ss_instagram', 'option'))) : ?>
                    <a href="<?php esc_url( the_field('ss_instagram', 'option')); ?>" class="icon ss-instagram"><span class="screen-reader-text">Instagram</span></a>
                <?php endif; ?>
                <?php if ( !empty(get_field('ss_youtube', 'option'))) : ?>
                    <a href="<?php esc_url( the_field('ss_youtube', 'option')); ?>" class="icon ss-youtube"><span class="screen-reader-text">Youtube</span></a>
                <?php endif; ?>
            </div>
            <div class="copyright-notice">
                <p>&copy; Třinecké Železárny a.s., 2013–<?php echo date("Y"); ?>, všechna práva vyhrazena | <a href="http://archiv.ehutnik.cz">Archiv do r. 2017</a></p>
            </div>
        </div><!-- .site-info -->
    </footer><!-- #colophon -->
</div><!-- #page -->

<!-- build:js assets/js/vendor.js -->
    <!-- bower:js -->
    <!-- endbower -->
<!-- endbuild -->

<?php wp_footer(); ?>

<?php if ( is_home() && is_front_page() ) : ?>
<script>

(function( $ ) {

    $('.slider-obrazem').slick({
      dots: true,
      infinite: false,
      //mobileFirst: true,
      dots: false,
      slidesToShow: 4.2,
      arrows: true,
      prevArrow: '<button class="slick-prev slick-arrow slick-disabled" aria-label="Předchozí" type="button" aria-disabled="true" style="display: block;"><i>Předchozí</i></button>',
      nextArrow: '<button class="slick-next slick-arrow slick-disabled" aria-label="Next" type="button" aria-disabled="true" style="display: block;"><i>Další</i></button>',
      responsive: [
        {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3.2,
              slidesToScroll: 1,
              arrows: true
            }
          },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2.2,
            slidesToScroll: 1,
            arrows: false
          }
        },
        {
          breakpoint: 500,
          settings: {
            slidesToShow: 1.2,
            slidesToScroll: 1,
            arrows: false
          }
        }

      ]
    });

})( jQuery );


</script>

<?php endif; /* is_home && is_frontpage */ ?>

</body>
</html>
