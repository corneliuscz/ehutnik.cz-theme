# Copyright (C) 2017 Contact Form 7 Storage
# This file is distributed under the same license as the Contact Form 7 Storage package.
msgid ""
msgstr ""
"Project-Id-Version: Contact Form 7 Storage 1.6.0-dev\n"
"Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/git\n"
"POT-Creation-Date: 2017-03-02 07:53:14+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-01-08 09:55+0100\n"
"Language-Team: \n"
"X-Generator: Poedit 2.0.5\n"
"Last-Translator: \n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"Language: cs_CZ\n"

#: cf7-storage.php:176 cf7-storage.php:426
msgid "Entries"
msgstr "Záznamy"

#: cf7-storage.php:425 cf7-storage.php:717
msgid "Contact Form Entries"
msgstr "Záznamy z kontaktního formuláře"

#: cf7-storage.php:563
msgid "You are not allowed to move this item to Trash."
msgstr "Nemáte oprávnění přesunout tuto položku do koše."

#: cf7-storage.php:566
msgid "Error moving an item to Trash."
msgstr "Chyba při přesunu položky do koše."

#: cf7-storage.php:582
msgid "You are not allowed to restore this item from Trash."
msgstr "Nemáte oprávnění obnovit tuto položku z koše."

#: cf7-storage.php:585
msgid "Error in restoring an item from Trash."
msgstr "Chyba při obnovení položky z koše."

#: cf7-storage.php:602
msgid "You are not allowed to delete this entry."
msgstr "Nemáte oprávnění smazat tuto položku."

#: cf7-storage.php:605
msgid "Error in deleting an entry."
msgstr "Chyba při mazání položky."

#: cf7-storage.php:684
msgid "Missing entry ID."
msgstr "Chybí ID záznamu."

#: cf7-storage.php:724
msgid "Search results for \"%s\""
msgstr "Výsledky hledání pro \"%s\""

#: cf7-storage.php:737
msgid "Subscribe to plugin updates"
msgstr "Přihlaste se k odběru aktualizací modulu"

#: cf7-storage.php:740
msgid "Subscribe"
msgstr "Odebírat"

#: cf7-storage.php:752
msgid "Search Entries"
msgstr "Prohledat záznamy"

#: cf7-storage.php:771
msgid "This contact form submission doesn't exist!"
msgstr "Tento záznam odeslaného formuláře neexistuje!"

#: cf7-storage.php:800 cf7-storage.php:1044
msgctxt "No attachments found"
msgid "None"
msgstr "Žádné"

#: cf7-storage.php:808 inc/admin-list-view.php:65
msgid "Contact Form"
msgstr "Kontaktní formulář"

#: cf7-storage.php:816 cf7-storage.php:1006
msgid "From"
msgstr "Od"

#: cf7-storage.php:820 cf7-storage.php:1007
msgid "To"
msgstr "Komu"

#: cf7-storage.php:824 cf7-storage.php:1005 inc/admin-list-view.php:66
msgid "Date"
msgstr "Datum"

#: cf7-storage.php:832 cf7-storage.php:1008
msgid "Subject"
msgstr "Předmět"

#: cf7-storage.php:836
msgid "Message"
msgstr "Zpráva"

#: cf7-storage.php:843 cf7-storage.php:1010
msgid "Attachments"
msgstr "Přílohy"

#: cf7-storage.php:895
msgid "No field values were captured."
msgstr "Nebyly zachyceny žádná pole."

#: cf7-storage.php:904
msgid "Post"
msgstr "Příspěvek"

#: cf7-storage.php:911 cf7-storage.php:925 cf7-storage.php:1014
msgid "Referer"
msgstr "Odkazatel"

#: cf7-storage.php:930
msgid "Visit"
msgstr "Navštívit"

#: cf7-storage.php:937
msgid "User agent"
msgstr "User Agent"

#: cf7-storage.php:944 cf7-storage.php:1016
msgid "IP Address"
msgstr "IP adresa"

#: cf7-storage.php:984
msgid "Form Submission"
msgstr "Odeslání formuláře"

#: cf7-storage.php:986
msgid "Field Values"
msgstr "Hodnoty pole"

#: cf7-storage.php:988
msgid "Submission Details"
msgstr "Podrobnosti o odeslání"

#: cf7-storage.php:1009
msgid "Body"
msgstr "Tělo"

#: cf7-storage.php:1011
msgid "Form Name"
msgstr "Jméno formuláře"

#: cf7-storage.php:1012
msgid "Entry ID"
msgstr "ID záznamu"

#: cf7-storage.php:1013
msgid "Entry URL"
msgstr "URL záznamu"

#: cf7-storage.php:1015
msgid "User-agent"
msgstr "User-agent"

#: cf7-storage.php:1179
msgid ""
"Storage for Contact Form 7 requires the latest version of Contact Form 7 "
"plugin."
msgstr ""
"Storage for Contact Form 7 vyžaduje poslední verzi pluginu Contact Form 7."

#: cf7-storage.php:1190
msgid "Form Entries"
msgstr "Záznamy formuláře"

#: inc/admin-list-view.php:64
msgid "Entry"
msgstr "Záznam"

#: inc/admin-list-view.php:73
msgid ""
"All form submissions will appear here. Try submitting one of the forms on "
"the site."
msgstr ""
"Zde se zobrazí záznamy o všech odeslaných formulářích. Zkuste jeden "
"odeslat z formuláře na webu."

#: inc/admin-list-view.php:94
msgid "All"
msgstr "Vše"

#: inc/admin-list-view.php:125 inc/admin-list-view.php:176
msgid "Restore"
msgstr "Obnovit"

#: inc/admin-list-view.php:128 inc/admin-list-view.php:188
msgid "Delete Permanently"
msgstr "Smazat trvale"

#: inc/admin-list-view.php:130
msgid "Move to Trash"
msgstr "Přesunout do koše"

#: inc/admin-list-view.php:168
msgid "Preview"
msgstr "Náhled"

#: inc/admin-list-view.php:182 inc/admin-list-view.php:202
#: inc/admin-list-view.php:382
msgid "Export as CSV"
msgstr "Exportovat jako CSV"

#: inc/admin-list-view.php:196
msgid "View"
msgstr "Zobrazit"

#: inc/admin-list-view.php:208
msgid "Trash"
msgstr "Koš"

#: inc/admin-list-view.php:234
msgid "View this submission from %s"
msgstr "Zobrazit tento záznam od %s"

#: inc/admin-list-view.php:252
msgid "%s ago"
msgstr "Před %s"

#: inc/admin-list-view.php:302
msgid "All Contact Forms"
msgstr "Všechny kontaktní formuláře"

#: inc/admin-list-view.php:322
msgid "Filter"
msgstr "Filtrovat"

#: inc/admin-list-view.php:334
msgid "Empty Trash"
msgstr "Vysypat koš"

#: inc/admin-list-view.php:349
msgid "Comma delimited"
msgstr "Oddělený čárkou"

#: inc/admin-list-view.php:350
msgid "Semicolon delimited"
msgstr "Oddělený středníkem"

#: inc/admin-list-view.php:351
msgid "Tab delimited"
msgstr "Oddělený tabulátorem"

#: inc/admin-list-view.php:377
msgid "Choose the CSV delimiter character"
msgstr "Zvolte znak oddělovače v CSV"

#: lib/envato-automatic-plugin-update/envato-plugin-update.php:167
msgid "Failed to retreive plugin details from the Envato API."
msgstr "Nepodařilo se získat detaily pluginu z Envato API."

#: lib/envato-automatic-plugin-update/envato-plugin-update.php:176
msgid "New version of <strong>%s</strong> is available."
msgstr "Je k dispozici nová verze <strong>%s</strong> ."

#. Plugin Name of the plugin/theme
msgid "Contact Form 7 Storage"
msgstr "Contact Form 7 Storage"

#. Plugin URI of the plugin/theme
msgid "https://preseto.com/plugins/contact-form-7-storage"
msgstr "https://preseto.com/plugins/contact-form-7-storage"

#. Description of the plugin/theme
msgid ""
"Store all Contact Form 7 submissions (including attachments) in your "
"WordPress dashboard."
msgstr ""
"Uožte všechny zprávy odeslané přes Contact Form 7 (včetně příloh) do "
"vašeho WordPressu."

#. Author of the plugin/theme
msgid "Kaspars Dambis"
msgstr "Kaspars Dambis"

#. Author URI of the plugin/theme
msgid "https://kaspars.net"
msgstr "https://kaspars.net"
