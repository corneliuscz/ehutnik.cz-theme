��    A      $  Y   ,      �     �     �     �  S   �            "        6     F     S     j          �     �     �     �     �  	   �     �  &   �          &  6   4     k     x       	   �     �     �  
   �     �     �     �     �  0   �          8     W     \     d     l     t     �     �  P   �  Y    	     Z	     b	  	   u	     	     �	  +   �	     �	     �	  
   �	  
   �	     �	     �	     
  )   
  /   F
  4   v
     �
  2   �
  �  �
     �     �     �  i   �  	   ?     I     O     n     �     �  #   �     �     �     �     �     �                 &   <  &   c     �  4   �     �  	   �     �     �          )  	   ,     6     E     M     a  0   u     �     �     �     �  	   �     �     �     	     &  L   >  a   �  	   �     �  	     +        F  1   ^     �     �  
   �  
   �     �     �     �  *   �  7     3   E     y  2   �         &             6   0         3         +       	         5         9   <             %   $                 4                             "       >       .   ?   -          :       /   )                  @                7       2       '   !       (      ;       8   A              *   ,   #           1   =   
           %s ago All All Contact Forms All form submissions will appear here. Try submitting one of the forms on the site. Attachments Body Choose the CSV delimiter character Comma delimited Contact Form Contact Form 7 Storage Contact Form Entries Date Delete Permanently Empty Trash Entries Entry Entry ID Entry URL Error in deleting an entry. Error in restoring an item from Trash. Error moving an item to Trash. Export as CSV Failed to retreive plugin details from the Envato API. Field Values Filter Form Entries Form Name Form Submission From IP Address Kaspars Dambis Message Missing entry ID. Move to Trash New version of <strong>%s</strong> is available. No attachments foundNone No field values were captured. Post Preview Referer Restore Search Entries Search results for "%s" Semicolon delimited Storage for Contact Form 7 requires the latest version of Contact Form 7 plugin. Store all Contact Form 7 submissions (including attachments) in your WordPress dashboard. Subject Submission Details Subscribe Subscribe to plugin updates Tab delimited This contact form submission doesn't exist! To Trash User agent User-agent View View this submission from %s Visit You are not allowed to delete this entry. You are not allowed to move this item to Trash. You are not allowed to restore this item from Trash. https://kaspars.net https://preseto.com/plugins/contact-form-7-storage Project-Id-Version: Contact Form 7 Storage 1.6.0-dev
Report-Msgid-Bugs-To: http://wordpress.org/support/plugin/git
POT-Creation-Date: 2017-03-02 07:53:14+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-01-08 09:55+0100
Language-Team: 
X-Generator: Poedit 2.0.5
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: cs_CZ
 Před %s Vše Všechny kontaktní formuláře Zde se zobrazí záznamy o všech odeslaných formulářích. Zkuste jeden odeslat z formuláře na webu. Přílohy Tělo Zvolte znak oddělovače v CSV Oddělený čárkou Kontaktní formulář Contact Form 7 Storage Záznamy z kontaktního formuláře Datum Smazat trvale Vysypat koš Záznamy Záznam ID záznamu URL záznamu Chyba při mazání položky. Chyba při obnovení položky z koše. Chyba při přesunu položky do koše. Exportovat jako CSV Nepodařilo se získat detaily pluginu z Envato API. Hodnoty pole Filtrovat Záznamy formuláře Jméno formuláře Odeslání formuláře Od IP adresa Kaspars Dambis Zpráva Chybí ID záznamu. Přesunout do koše Je k dispozici nová verze <strong>%s</strong> . Žádné Nebyly zachyceny žádná pole. Příspěvek Náhled Odkazatel Obnovit Prohledat záznamy Výsledky hledání pro "%s" Oddělený středníkem Storage for Contact Form 7 vyžaduje poslední verzi pluginu Contact Form 7. Uožte všechny zprávy odeslané přes Contact Form 7 (včetně příloh) do vašeho WordPressu. Předmět Podrobnosti o odeslání Odebírat Přihlaste se k odběru aktualizací modulu Oddělený tabulátorem Tento záznam odeslaného formuláře neexistuje! Komu Koš User Agent User-agent Zobrazit Zobrazit tento záznam od %s Navštívit Nemáte oprávnění smazat tuto položku. Nemáte oprávnění přesunout tuto položku do koše. Nemáte oprávnění obnovit tuto položku z koše. https://kaspars.net https://preseto.com/plugins/contact-form-7-storage 