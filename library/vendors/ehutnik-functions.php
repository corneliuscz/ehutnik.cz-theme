<?php
/**
 * Functions suite for eHutnik.cz website – enhance website with content or
 * user functions
 *
 * @package eHutnik_1.0
 */

/**
 * CronJob for Currenty exchange rates
 * source: http://wordpress-sensei.cz/shortcode-kurzy-ceske-koruny/
 *
 * @param string $cronjobname Name of a Cron Job
 * @param string $function Name of a function to run
 * @return void
 */

add_action('cron_hodina', 'ehutnik_nactiKurzy');

function crn_activation() {
    if (!wp_next_scheduled('cron_hodina')) {
        wp_schedule_event(time(), 'hourly', 'cron_hodina');
        ehutnik_nactiKurzy();
    }
}

add_action('wp', 'crn_activation');

/**
 * Download currency exchange rates each hour (Cron Job)
 * source: http://wordpress-sensei.cz/shortcode-kurzy-ceske-koruny/
 * modified by vybiral@graphic-house.cz to use curl instead of file_get_contents
 *
 * @param void
 * @return void
 */

function ehutnik_nactiKurzy() {
    $meny_url = 'http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt';
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $meny_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $meny_data = $meny = explode("\n", curl_exec($curl));
    curl_close($curl);

      unset($meny_data[0]);
      unset($meny_data[1]);
    array_pop($meny_data);
    $data = array();
    foreach ($meny_data as $mena) {
        $parametry = explode("|", $mena);
        if ( $parametry[3] ) {
            $data[$parametry[3]]['pocet'] = $parametry[2];
            $data[$parametry[3]]['kurz'] = str_replace(',', '.', $parametry[4]);
        }
    }

    var_dump ($data);

    if ($data)
        update_option('kurzy', $data);
}

/**
 * Currency exchange rate shortcode
 * source: http://wordpress-sensei.cz/shortcode-kurzy-ceske-koruny/
 *
 * @param array $atts       Array of function parametres
 * @return float $return    Rounded exchange rate
 */

function ehutnik_menaFunc($atts) {
    extract(shortcode_atts(array(
                'castka' => 1,
                'z' => '',
                'do' => 'CZK',
                'desetiny' => 2
                    ), $atts));
    $return = "";
    $castka = str_replace(',', '.', $castka);
    $meny = get_option('kurzy');
    if ($do === 'CZK' and $z === 'CZK')
        $return = $castka;
    elseif ($do === 'CZK') {
        $kurz = $meny[$z]['kurz'];
        $pocet = $meny[$z]['pocet'];
        $return = $kurz * ($castka / $pocet);
    } elseif ($z === 'CZK') {
        $kurz = $meny[$do]['kurz'];
        $pocet = $meny[$do]['pocet'];
        $return = $castka / ($kurz / $pocet);
    }
    return number_format($return, $desetiny, ',', ' ');
}

add_shortcode('ehutnik_mena', 'ehutnik_menaFunc');


/**
 * Get user's info in a nicely laid out medailon
 * usage: [USER_MEDAILON user_id=1 title="custom description"]
 * @param  array $atts
 * @param  string $content
 * @return string
 */
function ehutnik_user_medailon_shortcode_handler($atts) {
    extract(shortcode_atts(array(
                'id' => 2,
                'description' => '',
                'email' => '',
                'class' => 'medailon-normal'
                ), $atts));

    $content = '';

    if( $user_data = get_userdata( $id ) ) {
        $user_foto = get_field('user_foto', 'user_'.$id);

        $content = '<div class="user-medailon '.esc_attr($class).'">';
        if ( $user_foto ) {
            $content .= '<div class="user-photo">'.wp_get_attachment_image( $user_foto['ID'], 'full' ).'</div>';
        } else {
            $content .= '<div class="user-photo">&nbsp;</div>';
        }
        $content .= '<div class="user-container">';
        if ( !empty($description) ) {
            $content .= '<div class="user-description">'.esc_attr($description).'</div>';
        }
        $content .= '<div class="user-name">'.$user_data->display_name.'</div>';
        if ( empty($email) ) {
            $content .= '<div class="user-email"><a href="mailto:'.$user_data->user_email.'" title="e-mail">'.$user_data->user_email.'</a></div>';
        } else {
            $content .= '<div class="user-email"><a href="mailto:'.$email.'" title="e-mail">'.$email.'</a></div>';
        }
        $content .= '</div>';
        $content .= '</div>';
    }

    return $content;
}
add_shortcode('user_medailon', 'ehutnik_user_medailon_shortcode_handler');

/**
 * Widget Kurzy_ehutnik
 *
 * @return void
 */
function ehutnik_widget_kurzy()
{
    register_widget('ehutnik_wkurzy');
}
add_action('widgets_init', 'ehutnik_widget_kurzy');

// Creating the widget
class ehutnik_wkurzy extends WP_Widget
{
    // Contructor for Widget
    public function __construct()
    {
        parent::__construct(

            // Base ID of our widget
            'ehutnik_kurzy',

            // Widget name will appear in UI
            __('eHutnik – Kurzy měn', 'ehutnik'),

            // Widget description
            array( 'description' => __('Kurzy měn pro eHutník (Polský Zlotý, EUR, USD). Výchozí zdrojová měna je CZK.', 'ehutnik'), )
            );
    }

    // Creating widget front-end
    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        $currencies = apply_filters('sanitize_text_field', $instance['currencies']);
        $symbols = apply_filters('sanitize_html', $instance['symbols']);
        $source = 'CZK';  // default values
        $targets = 'PLN|EUR|USD';  // default values
        $symbs = 'zł|€|$';
        $castka = 1;

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (! empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        if (! empty($currencies)) {
            $targets = $currencies;
        }
        if (! empty($symbols)) {
            $symbs = $symbols;
        }

        // Create and print the widget
        $result = "";
        $meny = get_option('kurzy');

        $targets_arr = explode ('|', $targets);
        $symbs_arr = explode ('|', $symbs);

        echo '<ul class="kurzy-container">';
        $counter = 0;
        foreach ($targets_arr as $to) {
          $kurz = $meny[$to]['kurz'];
          $pocet = $meny[$to]['pocet'];
          $result = $kurz * ($castka / $pocet);
          $result = number_format($result, 2, ',', ' ');
          if ( empty($symbs_arr[$counter]) ) {
              $symbol = $to;
          } else {
              $symbol = $symbs_arr[$counter];
          }
          echo '<li><strong class="kurz-symbol">'. $symbol .'</strong><span class="kurz-vysledek"> '.$result.'&nbsp;Kč</span></li>';
          $counter++;
        }
        echo '</ul>';

        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('Kurzy měn', 'ehutnik');
        }
        if (isset($instance[ 'currencies' ])) {
            $currencies = $instance[ 'currencies' ];
        } else {
            $currencies = __('PLN|EUR|USD', 'ehutnik');
        }
        if (isset($instance[ 'symbols' ])) {
            $symbols = $instance[ 'symbols' ];
        } else {
            $symbols = __('zł|€|$', 'ehutnik');
        }
        // Widget admin form ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('currencies'); ?>"><?php _e('Currencies:'); ?><br><small>ISO codes for currencies, separated by |</small></label>
            <input class="widefat" id="<?php echo $this->get_field_id('currencies'); ?>" name="<?php echo $this->get_field_name('currencies'); ?>" type="text" value="<?php echo esc_attr($currencies); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('symbols'); ?>"><?php _e('Currency symbols:'); ?><br><small>Symbols for currencies, separated by | and in same order as above</small></label>
            <input class="widefat" id="<?php echo $this->get_field_id('symbols'); ?>" name="<?php echo $this->get_field_name('symbols'); ?>" type="text" value="<?php echo esc_html($symbols); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['currencies'] = (! empty($new_instance['currencies'])) ? strip_tags($new_instance['currencies']) : '';
        $instance['symbols'] = (! empty($new_instance['symbols'])) ? strip_tags($new_instance['symbols']) : '';
        return $instance;
    }
} // Class ehutnik_wkurzy ends here



/**
 * Widget Svátky
 * Vychází z kódu pro Shortcode pro svátky z http://kybernaut.cz
 *
 * @return void
 */
function ehutnik_widget_svatky()
{
    register_widget('ehutnik_wsvatky');
}
add_action('widgets_init', 'ehutnik_widget_svatky');

// Creating the widget
class ehutnik_wsvatky extends WP_Widget
{
    // Contructor for Widget
    public function __construct()
    {
        parent::__construct(

            // Base ID of our widget
            'ehutnik_svatky',

            // Widget name will appear in UI
            __('eHutnik – Svátky', 'ehutnik'),

            // Widget description
            array( 'description' => __('Zobrazí jmeniny pro aktuální den.', 'ehutnik'), )
            );
    }

    // Creating widget front-end
    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);
        $message_before = apply_filters('sanitize_text_field', $instance['message_before']);
        $message_after = apply_filters('sanitize_text_field', $instance['message_after']);

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (! empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        $m = array();
        $m[1] = array( "",'Nový rok', 'Karina', 'Radmila', 'Diana', 'Dalimil', 'Tři králové', 'Vilma', 'Čestmír', 'Vladan', 'Břetislav', 'Bohdana', 'Pravoslav', 'Edita', 'Radovan', 'Alice', 'Ctirad', 'Drahoslav', 'Vladislav', 'Doubravka', 'Ilona', 'Běla', 'Slavomír', 'Zdeněk', 'Milena', 'Miloš', 'Zora', 'Ingrid', 'Otýlie', 'Zdislava', 'Robin', 'Marika');
        $m[2] = array( "",'Hynek', 'Nela a Hromnice', 'Blažej', 'Jarmila', 'Dobromila', 'Vanda', 'Veronika', 'Milada', 'Apolena', 'Mojmír', 'Božena', 'Slavěna', 'Věnceslav', 'Valentýn', 'Jiřina', 'Ljuba', 'Miloslava', 'Gizela', 'Patrik', 'Oldřich', 'Lenka', 'Petr', 'Svatopluk', 'Matěj', 'Liliana', 'Dorota', 'Alexandr', 'Lumír', 'Horymír');
        $m[3] = array( "",'Bedřich', 'Anežka', 'Kamil', 'Stela', 'Kazimír', 'Miroslav', 'Tomáš', 'Gabriela', 'Františka', 'Viktorie', 'Anděla', 'Řehoř', 'Růžena', 'Rút a Matylda', 'Ida', 'Elena a Herbert', 'Vlastimil', 'Eduard', 'Josef', 'Světlana', 'Radek', 'Leona', 'Ivona', 'Gabriel', 'Marián', 'Emanuel', 'Dita', 'Soňa', 'Taťána', 'Arnošt', 'Kvido');
        $m[4] = array( "",'Hugo', 'Erika', 'Richard', 'Ivana', 'Miroslava', 'Vendula', 'Heřman a Hermína', 'Ema', 'Dušan', 'Darja', 'Izabela', 'Julius', 'Aleš', 'Vincenc', 'Anastázie', 'Irena', 'Rudolf', 'Valérie', 'Rostislav', 'Marcela', 'Alexandra', 'Evžénie', 'Vojtěch', 'Jiří', 'Marek', 'Oto', 'Jaroslav', 'Vlastislav', 'Robert', 'Blahoslav');
        $m[5] = array( "",'Svátek práce', 'Zikmund', 'Alexej', 'Květoslav', 'Klaudie', 'Radoslav', 'Stanislav', 'Den vítězství', 'Ctibor', 'Blažena', 'Svatava', 'Pankrác', 'Servác', 'Bonifác', 'Žofie', 'Přemysl', 'Aneta', 'Nataša', 'Ivo', 'Zbyšek', 'Monika', 'Emil', 'Vladimír', 'Jana', 'Viola', 'Filip', 'Valdemar', 'Vilém', 'Maxim', 'Ferdinand', 'Kamila');
        $m[6] = array( "",'Laura', 'Jarmil', 'Tamara', 'Dalibor', 'Dobroslav', 'Norbert', 'Iveta', 'Medard', 'Stanislava', 'Gita', 'Bruno', 'Antonie', 'Antonín', 'Roland', 'Vít', 'Zbyněk', 'Adolf', 'Milan', 'Leoš', 'Květa', 'Alois', 'Pavla', 'Zdeňka', 'Jan', 'Ivan', 'Adriana', 'Ladislav', 'Lubomír', 'Petr a Pavel', 'Šárka');
        $m[7] = array( "",'Jaroslava', 'Patricie', 'Radomír', 'Prokop', 'Cyril a Metoděj', 'Mistr Jan Hus', 'Bohuslava', 'Nora', 'Drahoslava', 'Libuše a Amálie', 'Olga', 'Bořek', 'Markéta', 'Karolína', 'Jindřich', 'Luboš', 'Martina', 'Drahomíra', 'Čeněk', 'Ilja', 'Vítězslav', 'Magdaléna', 'Libor', 'Kristýna', 'Jakub', 'Anna', 'Věroslav', 'Viktor', 'Marta', 'Bořivoj', 'Ignác');
        $m[8] = array( "",'Oskar', 'Gustav', 'Miluše', 'Dominik', 'Kristián', 'Oldřiška', 'Lada', 'Soběslav', 'Roman', 'Vavřinec', 'Zuzana', 'Klára', 'Alena', 'Alan', 'Hana', 'Jáchym', 'Petra', 'Helena', 'Ludvík', 'Bernard', 'Johana', 'Bohuslav', 'Sandra', 'Bartoloměj', 'Radim', 'Luděk', 'Otakar', 'Augustýn', 'Evelína', 'Vladěna', 'Pavlína');
        $m[9] = array( "",'Linda a Samuel', 'Adéla', 'Bronislav', 'Jindřiška', 'Boris', 'Boleslav', 'Regína', 'Mariana', 'Daniela', 'Irma', 'Denisa', 'Marie', 'Lubor', 'Radka', 'Jolana', 'Ludmila', 'Naděžda', 'Kryštof', 'Zita', 'Oleg', 'Matouš', 'Darina', 'Berta', 'Jaromír', 'Zlata', 'Andrea', 'Jonáš', 'Václav', 'Michal', 'Jeroným');
        $m[10] = array( "",'Igor', 'Olívie a Oliver', 'Bohumil', 'František', 'Eliška', 'Hanuš', 'Justýna', 'Věra', 'Štefan a Sára', 'Marina', 'Andrej', 'Marcel', 'Renáta', 'Agáta', 'Tereza', 'Havel', 'Hedvika', 'Lukáš', 'Michaela', 'Vendelín', 'Brigita', 'Sabina', 'Teodor', 'Nina', 'Beáta', 'Erik', 'Šarlota a Zoe', 'Vznik Československa', 'Silvie', 'Tadeáš', 'Štěpánka');
        $m[11] = array( "",'Felix', 'Památka zesnulých', 'Hubert', 'Karel', 'Miriam', 'Liběna', 'Saskie', 'Bohumír', 'Bohdan', 'Evžen', 'Martin', 'Benedikt', 'Tibor', 'Sáva', 'Leopold', 'Otmar', 'Mahulena', 'Romana', 'Alžběta', 'Nikola', 'Albert', 'Cecílie', 'Klement', 'Emílie', 'Kateřina', 'Artur', 'Xenie', 'René', 'Zina', 'Ondřej');
        $m[12] = array( "",'Iva', 'Blanka', 'Svatoslav', 'Barbora', 'Jitka', 'Mikuláš', 'Ambrož', 'Květoslava', 'Vratislav', 'Julie', 'Dana', 'Simona', 'Lucie', 'Lýdie', 'Radana', 'Albína', 'Daniel', 'Miloslav', 'Ester', 'Dagmar', 'Natálie', 'Šimon', 'Vlasta', 'Adam a Eva , Štědrý den', '1. sv. vánoční', 'Štěpán, 2. sv. vánoční', 'Žaneta', 'Bohumila', 'Judita', 'David', 'Silvestr');

        $dnes = getdate(current_time( 'timestamp', 0 ));
        $oslavenec = $m[$dnes['mon']][$dnes['mday']];

        $aj = array("January","February","March","April","May","June","July","August","September","October","November","December");
        $cz = array("ledna","února","března","dubna","května","června","července","srpna","září","října","listopadu","prosince");
        $hezkedatum = str_replace($aj, $cz, date("j. F Y"));

        echo '<ul>';
        echo '<li>'.$hezkedatum.' / <span class="hide-on-small">'.$message_before.'</span> <strong>'.$oslavenec.'</strong> '.$message_after.'</li>';
        echo '</ul>';

        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('Svátek', 'ehutnik');
        }
        if (isset($instance[ 'message_before' ])) {
            $message_before = $instance[ 'message_before' ];
        } else {
            $message_before = __('Dnes má svátek', 'ehutnik');
        }
        if (isset($instance[ 'message_after' ])) {
            $message_after = $instance[ 'message_after' ];
        } else {
            $message_after = __('.', 'ehutnik');
        }
        // Widget admin form ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('message_before'); ?>"><?php _e('Text před jménem oslavence:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('message_before'); ?>" name="<?php echo $this->get_field_name('message_before'); ?>" type="text" value="<?php echo esc_attr($message_before); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('message_after'); ?>"><?php _e('Text za jménem oslavence:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('message_after'); ?>" name="<?php echo $this->get_field_name('message_after'); ?>" type="text" value="<?php echo esc_attr($message_after); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['message_before'] = (! empty($new_instance['message_before'])) ? strip_tags($new_instance['message_before']) : '';
        $instance['message_after'] = (! empty($new_instance['message_after'])) ? strip_tags($new_instance['message_after']) : '';
        return $instance;
    }
} // Class ehutnik_wkurzy ends here


/**
 * Custom validation for WPCF7 forms
 * source: https://contactform7.com/2015/03/28/custom-validation/
 *
 * @param $tag validated item of a form
 * @return $result Result of validation
 */

add_filter( 'wpcf7_validate_number*', 'custom_ad_height_validation_filter', 20, 2 );

function custom_ad_height_validation_filter( $result, $tag ) {
        if ( 'inzerat-vyska' == $tag->name ) {
        $ad_height = isset( $_POST['inzerat-vyska'] ) ? trim( $_POST['inzerat-vyska'] ) : '';
        $ad_height_modulo = $ad_height % 4;

        if ( $ad_height_modulo != 0 ) {
            $result->invalidate( $tag, "Výška inzerátu není dělitelná 4, opravte prosím rozměr" );
        }
    }

    return $result;
}

/*
 * Remove "read more" text for content
 */
function ehutnik_modify_read_more_link() {
    //return '<a class="more-link" href="' . get_permalink() . '">Your New Text</a>';
    return null;
}
add_filter( 'the_content_more_link', 'ehutnik_modify_read_more_link' );

/*
 * Remove junk from <body> and .post classes
 * source: https://wordpress.stackexchange.com/a/15878
 */
/*
add_filter( 'body_class', 'ehutnik_body_class', 10, 2 );

function ehutnik_body_class( $wp_classes, $extra_classes ) {

    // List of the only WP generated classes allowed
    $whitelist = array( 'home', 'single', 'post', 'page', 'archive', 'category', 'error404', 'search', 'hfeed' );

    // Filter the body classes
    $wp_classes = array_intersect( $wp_classes, $whitelist );

    // Add the extra classes back untouched
    return array_merge( $wp_classes, (array) $extra_classes );
}

add_filter( 'post_class', 'ehutnik_post_class', 10, 2 );

function ehutnik_post_class( $wp_classes, $extra_classes ) {

    // List of the only WP generated classes allowed
    $whitelist = array( 'post', 'page', 'sticky', 'hentry' );

    // Filter the body classes
    $wp_classes = array_intersect( $wp_classes, $whitelist );

    // Add the extra classes back untouched
    return array_merge( $wp_classes, (array) $extra_classes );
}
*/

/**
 * Return an alternate title, without prefix, for every type used in the get_the_archive_title().
 */
add_filter('get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_year() ) {
        $title = get_the_date( _x( 'Y', 'yearly archives date format' ) );
    } elseif ( is_month() ) {
        $title = get_the_date( _x( 'F Y', 'monthly archives date format' ) );
    } elseif ( is_day() ) {
        $title = get_the_date( _x( 'F j, Y', 'daily archives date format' ) );
    } elseif ( is_tax( 'post_format' ) ) {
        if ( is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title' );
        }
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    } else {
        $title = __( 'Archives' );
    }
    return $title;
});

/**
 * Skryj přípěvky Inzerce, Dobrá rada a Krátké zrávy z archivu autora
 */
function ehutnik_custom_query_vars( $query ) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ( is_author() ) {

        // nechceme přípěvky ve formátu Status
        $taxquery = array(
            array(
                'taxonomy' => 'post_format',
                'field' => 'slug',
                'terms' => array( 'post-format-status' ),
                'operator' => 'NOT IN'
            )
        );
        $query->set( 'tax_query', $taxquery );

        // Příspěvky z kategorie 4 (Dobré zprávy) a 12 (Inzerce) nechceme
        $query->set( 'category__not_in', array(4, 12) );

        // Příspěvky s jiným autorem (vlastním) nechceme
        $metaquery = $query->get('meta_query');
        $metaquery[] = array(
            'relation' => 'OR',
            array(
                'key' => 'jiny_autor',
                'value' => '0',
                'compare' => '=='
            ),
            array(
                'key' => 'jiny_autor',
                'compare' => 'NOT EXISTS'
            )
        );
        $query->set( 'meta_query', $metaquery );
    }
  }
  return $query;
}
add_action( 'pre_get_posts', 'ehutnik_custom_query_vars' );

/**
 * Add custom buttons to TinyMCE editor
 */

function ehutnik_add_more_buttons($buttons) {
    //$buttons[] = 'hr';
    //$buttons[] = 'del';
    //$buttons[] = 'sub';
    //$buttons[] = 'sup';
    //$buttons[] = 'fontselect';
    //$buttons[] = 'fontsizeselect';
    //$buttons[] = 'cleanup';
    //$buttons[] = 'styleselect';
    $buttons[] = 'superscript';    /* Add superscript button */
    $buttons[] = 'subscript';    /* Add subscript button */
    return $buttons;
}
add_filter("mce_buttons_2", "ehutnik_add_more_buttons");


/*
 * Hook more images to OG:Image tag from ACF Gallery in regular and Obrazem posts
 *
 * Depends on ACF and Facebook Open Graph, Google+ and Twitter Card Tags plugin
 * https://cs.wordpress.org/plugins/advanced-custom-fields/
 * https://cs.wordpress.org/plugins/wonderm00ns-simple-facebook-open-graph-tags/
 */
if( class_exists('acf') ) :
    function ehutnik_og_gallery( $fb_image_additional ) {

        if ( is_single() ) {
            $images = get_field('galerie');

            if ($images) {

                if ( !is_array( $fb_image_additional ) ) {
                    $fb_image_additional = array();
                }
                foreach ($images as $image) {
                    $fb_image_additional[] = array(
                        'fb_image' => $image['url'],
                        'png_overlay' => false, //Overlay PNG
                    );
                }
            }
            return $fb_image_additional;
        }
    }

    add_filter('fb_og_image_additional', 'ehutnik_og_gallery');
endif;

/**
 * Load active posts from "Inzerce/PR články" category and show one random of them
 */

function ehutnik_showArticleAds() {
    $date_now = date('Y-m-d H:i:s');
    $ads_args = array (
        'post_status' => 'publish',
        'posts_per_page'     => 1,
        'cat'                => '12',        // Příspěvky z kategorie 12 – Inzerce
        'orderby'            => 'rand',
        'meta_query'         => array(
            array(
                'key'            => 'prclanek_expirace',
                'compare'        => '>=',
                'value'          => $date_now,
                'type'           => 'DATETIME'
            )
        ),
    );

    $ads_posts = new WP_Query( $ads_args );

    if ( $ads_posts->have_posts() ) {
        while ( $ads_posts->have_posts() ) {
            $ads_posts->the_post();
            get_template_part( 'template-parts/content', get_post_format() );
        }
    }
    wp_reset_postdata();
}
