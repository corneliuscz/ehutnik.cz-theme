<?php
/**
 *
 * Extend Advanced Ads plugin with custom AD types and other things
 *
 * @package eHutnik
 * @author Josef Vybíral
 *
 */

/**
 * Hook a new Ad type into Advanced Ads
 */

if( class_exists('Advanced_Ads') ) {

	function ehutnik_pr_ad_type(){

	/**
	 * Advanced Ads Content Ad Type
	 *
	 * @package   Advanced_Ads
	 * @author    Thomas Maier <thomas.maier@webgilde.com>
	 * @license   GPL-2.0+
	 * @link      http://webgilde.com
	 * @copyright 2014 Thomas Maier, webgilde GmbH
	 *
	 * Class containing information about the content ad type
	 * this should also work as an example for other ad types
	 *
	 * see also includes/ad-type-abstract.php for basic object
	 *
	 */
	class Advanced_Ads_Ad_Type_PRContent extends Advanced_Ads_Ad_Type_Abstract{

		/**
		 * ID - internal type of the ad type
		 *
		 * must be static so set your own ad type ID here
		 * use slug like format, only lower case, underscores and hyphens
		 *
		 * @since 1.0.0
		 */
		public $ID = 'prarticle';

		/**
		 * set basic attributes
		 *
		 * @since 1.0.0
		 */
		public function __construct() {
			$this->title = __( 'Advertisement Article', 'ehutnik' );
			$this->description = __( 'Select PR article that you want displayed as an advertisement. Articles are selected form the "PR články" category', 'ehutnik' );
			$this->parameters = array(
				'article_id' => NULL
			);
		}

		/**
		 * output for the ad parameters metabox
		 *
		 * this will be loaded using ajax when changing the ad type radio buttons
		 * echo the output right away here
		 * name parameters must be in the "advanced_ads" array
		 *
		 * @param obj $ad ad object
		 * @since 1.0.0
		 */
		public function render_parameters($ad){
			// load content
			$article_id = (isset($ad->output['article_id'])) ? $ad->output['article_id'] : '';

			?><p class="description"><?php _e( 'Select Advertisement article.', 'ehutnik' ); ?></p>
		    <div><label for="advads-article-id"><?php _e('Article ID', 'ehutnik'); ?></label>:
			<select name="advanced_ad[output][article_id]" id="advads-article-id">
			<?php
				// Get all posts from PR Článek category
				global $post;
				$args = array(
					'numberposts' 	=> -1,		// All
					'cat' 			=> '12',	// pr-clanky category
    				'orderby'   	=> array(
      						'date'	=> 'ASC'
     					)
					);
				$posts = get_posts($args);

				// Build dropdown to select posts from
				foreach( $posts as $post ) : setup_postdata($post); ?>
					<option value="<?php echo $post->ID; ?>" <?php selected( $post->ID, $article_id); ?>><?php the_title(); ?></option>
			<?php endforeach;
				// Cleanup the mess in post data
				wp_reset_postdata();
			?>
		</select></div><br><hr/>
			<?php include ADVADS_BASE_PATH . 'admin/views/ad-info-after-textarea.php';
		}

		/**
		 * prepare the ads frontend output
		 *
		 * @param obj $ad ad object
		 * @return str $content ad content prepared for frontend output
		 * @since 1.0.0
		 */
		public function prepare_output($ad){
			$article_id = ( isset( $ad->output['article_id'] ) ) ? absint( $ad->output['article_id'] ) : FALSE;

			if ( $article_id ) {
				// get global post object
				global $post;
				$post = get_post($article_id);
				setup_postdata($post);

				ob_start();	// Catch the object data
				get_template_part( 'template-parts/content', get_post_format() );	// Render the post into object

				wp_reset_postdata();	// Reset post data mess
			}

			// Return rendered post
			return ob_get_clean();
		}

	}

	$types['plain'] = new Advanced_Ads_Ad_Type_Plain(); /* plain text and php code */
	//$types['dummy'] = new Advanced_Ads_Ad_Type_Dummy(); /* dummy ad */
	$types['content'] = new Advanced_Ads_Ad_Type_Content(); /* rich content editor */
	$types['image'] = new Advanced_Ads_Ad_Type_Image(); /* image ads */
	$types['group'] = new Advanced_Ads_Ad_Type_Group(); /* group ad */
	$types['prarticle'] = new Advanced_Ads_Ad_Type_PRContent(); /* rich PR content editor */
	return $types;
	}

add_filter( 'advanced-ads-ad-types', 'ehutnik_pr_ad_type' );
} // !if class exists
