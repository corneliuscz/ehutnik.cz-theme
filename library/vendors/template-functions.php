<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package eHutnik_1.0
 */

/**
 * Custom print stylesheets
 */

function ehutnik_print_styles(){
    wp_enqueue_style(
        'ehutnik-print-style',
        get_stylesheet_directory_uri() . '/assets/css/print.css',
        array(),
        '20171124',
        'print' // print styles only
    );
}
add_action( 'wp_enqueue_scripts', 'ehutnik_print_styles' );

/**
 * Custom editor styles
 */

add_editor_style( '/assets/css/editor.css' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function ehutnik_body_classes( $classes ) {
    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    return $classes;
}
add_filter( 'body_class', 'ehutnik_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function ehutnik_pingback_header() {
    if ( is_singular() && pings_open() ) {
        echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
    }
}
add_action( 'wp_head', 'ehutnik_pingback_header' );


/**
* filter function to force wordpress to add our custom srcset values
* @param array  $sources {
*     One or more arrays of source data to include in the 'srcset'.
*
*     @type type array $width {
*          @type type string $url        The URL of an image source.
*          @type type string $descriptor The descriptor type used in the image candidate string,
*                                        either 'w' or 'x'.
*          @type type int    $value      The source width, if paired with a 'w' descriptor or a
*                                        pixel density value if paired with an 'x' descriptor.
*     }
* }
* @param array  $size_array    Array of width and height values in pixels (in that order).
* @param string $image_src     The 'src' of the image.
* @param array  $image_meta    The image meta data as returned by 'wp_get_attachment_metadata()'.
* @param int    $attachment_id Image attachment ID.

* @author: Aakash Dodiya
* @website: http://www.developersq.com
*/
add_filter( 'wp_calculate_image_srcset', 'ehutnik_add_custom_image_srcset', 10, 5 );

function ehutnik_add_custom_image_srcset( $sources, $size_array, $image_src, $image_meta, $attachment_id ){

    // image base name
    $image_basename = wp_basename( $image_meta['file'] );
    // upload directory info array
    $upload_dir_info_arr = wp_get_upload_dir();
    // base url of upload directory
    $baseurl = $upload_dir_info_arr['baseurl'];

    // Uploads are (or have been) in year/month sub-directories.
    if ( $image_basename !== $image_meta['file'] ) {
        $dirname = dirname( $image_meta['file'] );

        if ( $dirname !== '.' ) {
            $image_baseurl = trailingslashit( $baseurl ) . $dirname;
        }
    }

    $image_baseurl = trailingslashit( $image_baseurl );
    // check whether our custom image size exists in image meta
    if( array_key_exists('thumb-tiny', $image_meta['sizes'] ) ){

        // add source value to create srcset
        $sources[ $image_meta['sizes']['thumb-tiny']['width'] ] = array(
                 'url'        => $image_baseurl .  $image_meta['sizes']['thumb-tiny']['file'],
                 'descriptor' => 'w',
                 'value'      => $image_meta['sizes']['thumb-tiny']['width'],
        );
    }
    if( array_key_exists('thumb-medium-small', $image_meta['sizes'] ) ){

        // add source value to create srcset
        $sources[ $image_meta['sizes']['thumb-medium-small']['width'] ] = array(
                 'url'        => $image_baseurl .  $image_meta['sizes']['thumb-medium-small']['file'],
                 'descriptor' => 'w',
                 'value'      => $image_meta['sizes']['thumb-medium-small']['width'],
        );
    }

    //return sources with new srcset value
    return $sources;
}

/**
 * Change the default "sizes" attribute created by WordPress
 * for the content archive thumbnails
 */

add_filter( 'wp_calculate_image_sizes', 'ehutnik_thumbnail_image_sizes', 10, 5 );

function ehutnik_thumbnail_image_sizes( $sizes, $size, $image_src, $image_meta, $attachment_id ) {
    //if( is_archive() && is_main_query() || is_home() ) {
    if( is_home() ) {
        $sizes = '(min-width: 768px) 768px, (min-width: 480px) 320px, (min-width: 320px) 320px, (min-width: 160px) 160px, 100wv';
    }
    return $sizes;
}
/*
function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}

// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );

// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );
*/


/**
 * Remove inlined max-width from captions
 *
 * Source: http://wp-snippets.com/remove-caption-inline-width/
 * Modified: Two [ brackets were replaced by html entity (&#91;) in preg_match
 */

add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');
function fixed_img_caption_shortcode($attr, $content = null) {
    // New-style shortcode with the caption inside the shortcode with the link and image tags.
    if ( ! isset( $attr['caption'] ) ) {
        if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
            $content = $matches[1];
            $attr['caption'] = trim( $matches[2] );
        }
    }

    // Allow plugins/themes to override the default caption template.
    $output = apply_filters('img_caption_shortcode', '', $attr, $content);
    if ( $output != '' )
        return $output;

    extract(shortcode_atts(array(
        'id'    => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => ''
    ), $attr));

    if ( 1 > (int) $width || empty($caption) )
        return $content;

    if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

    return '<figure ' . $id . 'class="wp-caption ' . esc_attr($align) . '">'
    . do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . $caption . '</figcaption></figure>';
}


/**
 *  Wrap images without caption (wrapped in <P>) with <figure>
 *
 *  Source: https://wordpress.stackexchange.com/questions/174582/always-use-figure-for-post-images
 */

add_filter('the_content', function ($content) {
    # Prevent errors so we can parse HTML5
    libxml_use_internal_errors(true); # https://stackoverflow.com/questions/9149180/domdocumentloadhtml-error

    # Load the content
    $dom = new DOMDocument();

    # With UTF-8 support
    # https://stackoverflow.com/questions/8218230/php-domdocument-loadhtml-not-encoding-utf-8-correctly
    $dom->loadHTML('<?xml encoding="utf-8" ?>' . $content);

    # Find all images
    $images = $dom->getElementsByTagName('img');

    # Go through all the images
    foreach ($images as $image) {
        $child = $image; # Store the child element
        $wrapper = $image->parentNode; # And the wrapping element

        # If the image is linked
        if ($wrapper->tagName == 'a') {
            $child = $wrapper; # Store the link as the child
            $wrapper = $wrapper->parentNode; # And its parent as the wrapper
        }

        # If the parent is a <p> - replace it with a <figure>
        if ($wrapper->tagName == 'p') {
            $figure = $dom->createElement('figure');

            $figure->setAttribute('class', $image->getAttribute('class')); # Give figure same class as img
            $image->setAttribute('class', ''); # Remove img class
            $figure->appendChild($child); # Add img to figure
            $wrapper->parentNode->replaceChild($figure, $wrapper); # Replace <p> with <figure>
        }
    }

    # Turn on errors again...
    libxml_use_internal_errors(false);

    # Strip DOCTYPE etc from output
    return str_replace(['<body>', '</body>'], '', $dom->saveHTML($dom->getElementsByTagName('body')->item(0)));
}, 99);



/**
 * Share this article function
 *
 */

function ehutnik_share_this() {
    if(!is_feed() && !is_home()) {

        $text_pred = "Přečtěte si na ehutnik.cz: ";
        $clanek_url = urlencode(get_permalink());
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $clanek_url ?>" title="Sdílej na Facebook"><i class="icon share-facebook"></i><span class="screen-reader-text">Sdílej na Facebook</span></a>
        <a href="https://twitter.com/intent/tweet?text=<?php echo the_title($text_pred, '', TRUE);?> na <?php echo $clanek_url ?>" title="Sdílej na twitter"><i class="icon share-twitter"></i><span class="screen-reader-text">Sdílej na twitter</span></a>
        <a href="mailto:?subject=<?php echo the_title($text_pred, '', TRUE);?>&body=<?php echo the_title($text_pred.'%0d', '', TRUE);?> na <?php echo $clanek_url ?>" title="Sdílej e-mailem"><i class="icon share-email"></i><span class="screen-reader-text">Sdílej e-mailem</span></a>
        <a href="#" title="Vytiskni článek" onclick="window.print()"><i class="icon share-print"></i><span class="screen-reader-text">Vytiskni článek</span></a>
        <?php
    }
}


/**
 * Show gallery lightbox from a post data
 * $images    array of images from gallery created by ACF gallery
 * return     echo lightbox data
 */

function ehutnik_lightbox( $images ) {

	$lightbox = '<div class="lightbox-bg lightbox-bg-gallery" style="display: none;">';
	$lightbox .= '<div class="post-gallery-container">';
	$lightbox .= '<span id="gallery-close"><span class="screen-reader-text">zavřít galerii</span></span>';
	$lightbox .= '<div class="post-gallery-view">';
	$lightbox .= '<ul id="gallery" class="gallery-images">';

	foreach ( $images as $image ) {
		$dimensions = wp_get_attachment_image_src($image['ID'], 'full');
		if ( $dimensions[1] > $dimensions[2] ) { $direction = ' class="img-landscape"'; }
		else { $direction = ' class="img-portrait"'; }

		$lightbox .= '<li><figure'.$direction.'>';
		$lightbox .= wp_get_attachment_image( $image['ID'], 'full', false, array( 'class' => 'no-lazyload' ) );
		$lightbox .= '<figcaption>'.$image['description'].'</figcaption>';
		$lightbox .= '</figure></li>';
	}

	$lightbox .= '</ul>';
	$lightbox .= '</div>';
	$lightbox .= '<div class="post-gallery-thumbs">';
	$lightbox .= '<ul id="gallery-thumbs" class="gallery-thumbs-list">';

	foreach ( $images as $image ) {
		$dimensions = wp_get_attachment_image_src($image['ID'], 'thumbnail');
		if ( $dimensions[1] > $dimensions[2] ) { $direction = ' bx-landscape'; }
		else { $direction = ' bx-portrait'; }

		$lightbox .= '<li class="thumb-item"><div class="thumb'.$direction.'"><a href="'.$image['url'].'" rel="lightbox">';
		$lightbox .= wp_get_attachment_image( $image['ID'], 'thumbnail', false, array( 'class' => 'no-lazyload' ) );
		$lightbox .= '</a></div></li>';
	}

	$lightbox .= '</ul>';
	$lightbox .= '</div>';
	$lightbox .= '</div><!-- .gallery-container -->';
	$lightbox .= '</div><!-- .lightbox-bg -->';

	return $lightbox;
}

function ehutnik_lightbox_single() {

	$lightbox = '<div class="lightbox-bg lightbox-bg-single" style="display: none;">';
	$lightbox .= '<div class="post-gallery-container post-gallery-container-single">';
	$lightbox .= '<span id="gallery-close"><span class="screen-reader-text">zavřít galerii</span></span>';
	$lightbox .= '<div class="post-gallery-view">';
	$lightbox .= '<ul class="gallery-images">';

	$thumbSrc = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full', false );
	if ( $thumbSrc[1] > $thumbSrc[2] ) { $direction = ' class="img-landscape"'; }
	else { $direction = ' class="img-portrait"'; }

	$lightbox .= '<li><figure'.$direction.'>';
	$lightbox .= wp_get_attachment_image( get_post_thumbnail_id(), 'full', false, array( 'class' => 'no-lazyload' ) );

	if ( $caption = get_post( get_post_thumbnail_id() )->post_content ) {
		$lightbox .= '<figcaption>'.$caption.'</figcaption>';
	}

	$lightbox .= '</figure></li>';

	$lightbox .= '</ul>';
	$lightbox .= '</div>';
	$lightbox .= '</div><!-- .gallery-container -->';
	$lightbox .= '</div><!-- .lightbox-bg -->';

	return $lightbox;
}

/**
 * Responsively embeded videos
 * Add container automagically
 * Ratio: 16:9
 */
function alx_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'alx_embed_html' ); // Jetpack
