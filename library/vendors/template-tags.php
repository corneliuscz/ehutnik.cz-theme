<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package eHutnik_1.0
 */

if ( ! function_exists( 'ehutnik_posted_on' ) ) :
    /**
     * Prints HTML with meta information for the current post-date/time and author.
     */
    function ehutnik_posted_on() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date() ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date() )
        );

        $posted_on = sprintf(
            /* translators: %s: post date. */
            esc_html_x( 'Posted on %s', 'post date', 'ehutnik' ),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );

        $byline = sprintf(
            /* translators: %s: post author. */
            esc_html_x( 'by %s', 'post author', 'ehutnik' ),
            '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
        );

        echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

    }
endif;

if ( ! function_exists( 'ehutnik_entry_footer' ) ) :
    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function ehutnik_entry_footer() {
        // Hide category and tag text for pages.
        if ( 'post' === get_post_type() ) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list( esc_html__( ', ', 'ehutnik' ) );
            if ( $categories_list ) {
                /* translators: 1: list of categories. */
                printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'ehutnik' ) . '</span>', $categories_list ); // WPCS: XSS OK.
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'ehutnik' ) );
            if ( $tags_list ) {
                /* translators: 1: list of tags. */
                printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'ehutnik' ) . '</span>', $tags_list ); // WPCS: XSS OK.
            }
        }

        if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
            echo '<span class="comments-link">';
            comments_popup_link(
                sprintf(
                    wp_kses(
                        /* translators: %s: post title */
                        __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'ehutnik' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    get_the_title()
                )
            );
            echo '</span>';
        }

        edit_post_link(
            sprintf(
                wp_kses(
                    /* translators: %s: Name of current post. Only visible to screen readers */
                    __( 'Edit <span class="screen-reader-text">%s</span>', 'ehutnik' ),
                    array(
                        'span' => array(
                            'class' => array(),
                        ),
                    )
                ),
                get_the_title()
            ),
            '<span class="edit-link">',
            '</span>'
        );
    }
endif;


if ( ! function_exists( 'ehutnik_entry_meta' ) ) :
    /**
     * Prints HTML with meta information for the current post: date/time, category and tag.
     */
    function ehutnik_entry_meta() {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $date_published = get_the_date( 'Y-d-m' );
        $date_today = date( 'Y-d-m' );
        if ( $date_published === $date_today ) {
            $date_format = 'H:i';
        } elseif (is_single() && in_the_loop() ) {
            $date_format = 'H:i / j. n. Y';
        } else {
            $date_format = '';
        }

        $time_string = sprintf( $time_string,
            esc_attr( get_the_date( 'c' ) ),
            esc_html( get_the_date( $date_format ) ),
            esc_attr( get_the_modified_date( 'c' ) ),
            esc_html( get_the_modified_date( $date_format ) )
        );

        $posted_on = sprintf(
            /* translators: %s: post date. */
            esc_html_x( '%s', 'post date', 'ehutnik' ),
            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
        );

        if ( 'post' === get_post_type() ) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list( esc_html__( ', ', 'ehutnik' ) );
            $categories = '';
            if ( $categories_list ) {
                if ( !is_single() || ( is_single() && !in_the_loop() ) ) { $sep_cat = ' / '; } else { $sep_cat = ''; }
                /* translators: 1: list of categories. */
                $categories = sprintf( '%2$s<span class="cat-links">' . esc_html__( '%1$s', 'ehutnik' ) . '</span>', $categories_list, $sep_cat ); // WPCS: XSS OK.
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'ehutnik' ) );
            $tags = '';
            if ( $tags_list ) {
                if ( !is_single() || ( is_single() && !in_the_loop() ) ) { $sep1 = ' / '; } else { $sep1 = ''; }
                /* translators: 1: list of tags. */
                $tags = sprintf( '%2$s<span class="tags-links">' . esc_html__( '%1$s', 'ehutnik' ) . '</span>', $tags_list, $sep1 ); // WPCS: XSS OK.
            }

            $camera = '';
            if ( has_post_format('gallery') ) {
                  // If the post is a gallery (Obrazem) then show an icon of camera
                  $camera = '<i class="icon camera"></i>';
            }
        }

        $pr_article = strpos($categories, 'inzerce');

        if ( is_single() && in_the_loop() ) {
            $topic = $tags ? $tags : $categories;
            echo '<strong>' . $topic . '</strong> / <span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.
        } elseif ( $pr_article ) {
            echo str_replace('/ ', '', $categories) ; // WPCS: XSS OK
        } else {
            echo '<span class="posted-on">' . $posted_on . '</span>' . $categories . '' . $tags . $camera ; // WPCS: XSS OK.
        }


    }
endif;

/*
if ( ! function_exists( 'ehutnik_posted_by' ) ) :

    // Print Author link with archive URL
    function ehutnik_posted_by() {
        echo '<a href="'.get_author_posts_url( get_the_author_meta( 'ID' ) ).'" title="Příspěvky od '.get_the_author_meta('user_nicename', get_the_author_meta( 'ID' ) ).'" rel="author">'.get_the_author_meta('nickname', get_the_author_meta( 'ID' ) ).'</a>';
    };

endif;
*/

if ( ! function_exists( 'ehutnik_posted_by' ) ) :

    // Print Author link with archive URL
    function ehutnik_posted_by() {
        // Get some global variables so we know what post we are working with
        global $post, $post_id;

        // Echo "Inzerce" instead of author info if the post is an AD.
        $inzertni = FALSE;
        // Echo nothing for post from category "Dobré rady"
        $dobrarada = FALSE;
        // Echo custom author for posts with meta field filled in
        $jinyautor = FALSE;

        // Find out in which category are we and set variables properly
        $terms = get_the_terms($post->ID, 'category');
        foreach ($terms as $term) {
            if ($term->term_id == 12) {
                $inzertni = TRUE;
            } elseif ($term->term_id == 4) {
                $dobrarada = TRUE;
            }
        }

        // Find if the 'Other author' has been selected and value inserted then set the proper variable
        $autor_podminka = get_field( 'jiny_autor', $post->ID );
        if ( $autor_podminka ) {
            $autor_jmeno = get_field( 'jiny_autor_jmeno' );
            if ( $autor_jmeno ) {
                $jinyautor = $autor_jmeno;
            }
        }

        if ( $inzertni && !$jinyautor ) {
            echo '<span class="ehutnik-adlabel">Inzerce</span>';
        }
        elseif ( $dobrarada && !$jinyautor ) {
            echo '';
        }
        elseif ( ( $inzertni && $jinyautor ) || ( $dobrarada && $jinyautor ) || $jinyautor ) {
            echo '<span class="prispevatel">'.$jinyautor.'</span>';
        }
        else {
            echo '<a href="'.get_author_posts_url( get_the_author_meta( 'ID' ) ).'" title="Příspěvky od '.get_the_author_meta('user_nicename', get_the_author_meta( 'ID' ) ).'" rel="author">'.get_the_author_meta('nickname', get_the_author_meta( 'ID' ) ).'</a>';
        }
    };

endif;
