<?php
/**
 * Definitions for custom post fields (Advanced Custom Fields Pro)
 * and custom options pages.
 *
 * @package eHutnik_1.0
 */

/*
 * Theme options
 *
 * requires Advanced Custom Fields Pro
 */

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'    => 'Nastavení eHutnik.cz',
        'menu_title'    => 'Nastavení eHutnik.cz',
        'menu_slug'     => 'ehutnik-general-settings',
        'capability'    => 'edit_private_posts',
    ));
}


/**
 * Custom Fields
 * – Post gallery
 * – Social links of eHutnik
 * – Author photo
 * – Ad forms
 * – Contact sections with contacts
 * - Custom expiration for homepage showcase for Advert articles
 * – Custom Author name for specific articles (Ads, Advices, external contributors)
 */


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5a05a8e5762c1',
    'title' => 'Galerie článku',
    'fields' => array(
        array(
            'key' => 'field_5a05a8fc157ec',
            'label' => 'Galerie článku',
            'name' => 'galerie',
            'type' => 'gallery',
            'instructions' => 'Na toto místo přetáhněte připravené fotografie pro galerii. Po nahrání je budete moct uspořádat, pojmenovat a popsat.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'min' => '',
            'max' => '',
            'insert' => 'append',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => 'jpg,png',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
            ),
            array(
                'param' => 'post_taxonomy',
                'operator' => '!=',
                'value' => 'post_format:post-format-status',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5a2161121ea74',
    'title' => 'Inzertní formuláře',
    'fields' => array(
        array(
            'key' => 'field_5a2161a766476',
            'label' => 'Formulář pro příjem inzerce',
            'name' => 'formular-inzerce',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => 'field_5a21621f66477',
            'min' => 0,
            'max' => 0,
            'layout' => 'table',
            'button_label' => '',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a21621f66477',
                    'label' => 'Nadpis formuláře',
                    'name' => 'inzerce_nadpis_formulare',
                    'type' => 'text',
                    'instructions' => 'Název sekce s formulářem, například "Tištěná inzerce v rubrikách Rodinná kronika, Blahopřání…"',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => 'Tištěná inzerce v rubrikách Rodinná kronika, Blahopřání…',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5a21629066478',
                    'label' => 'ID formuláře',
                    'name' => 'inzerce_id_formulare',
                    'type' => 'text',
                    'instructions' => 'ID formuláře vygenerovaný pluginem Contact Form 7',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5a2162e366479',
                    'label' => 'CSS kód ikony',
                    'name' => 'inzerce_ikona',
                    'type' => 'text',
                    'instructions' => 'Název ikony z CSS sprites, pokud jej znáte.',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
            ),
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'page',
                'operator' => '==',
                'value' => '11',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5a5cbe26da3aa',
    'title' => 'PR článek – nastavení',
    'fields' => array(
        array(
            'key' => 'field_5a5cc3575fb11',
            'label' => 'Zobrazit na HP do',
            'name' => 'prclanek_expirace',
            'type' => 'date_time_picker',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'display_format' => 'd. m. Y H:i:s',
            'return_format' => 'Y-m-d H:i:s',
            'first_day' => 1,
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_category',
                'operator' => '==',
                'value' => 'category:inzerce',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5a268db68b21b',
    'title' => 'Sekce kontaktů',
    'fields' => array(
        array(
            'key' => 'field_5a268dc4ce1bf',
            'label' => 'Sekce',
            'name' => 'kontakty-sekce',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => 'field_5a268dedce1c0',
            'min' => 0,
            'max' => 0,
            'layout' => 'row',
            'button_label' => '',
            'sub_fields' => array(
                array(
                    'key' => 'field_5a268dedce1c0',
                    'label' => 'Titulek sekce',
                    'name' => 'kontakty_sekce_titulek',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5a268e2fce1c1',
                    'label' => 'Obsah sekce',
                    'name' => 'kontakty_sekce_obsah',
                    'type' => 'wysiwyg',
                    'instructions' => 'Vložte shortkódy uživatelů, které chcete v této sekci zobrazit.
[user_medailon id=2 description="Šéfredaktorka pro Třinecký hutník" class="medailon-large"]

K dispozici jsou parametry: ID uživatele, popiska nad jménem (je možné vynechat), CSS třída velikosti (je možné vynechat)',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'tabs' => 'all',
                    'toolbar' => 'basic',
                    'media_upload' => 0,
                    'delay' => 0,
                ),
            ),
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'page_template',
                'operator' => '==',
                'value' => 'page-kontakty.php',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5a0ac3116d3f3',
    'title' => 'Sociální sítě',
    'fields' => array(
        array(
            'key' => 'field_5a0ac593698ed',
            'label' => 'Twitter eHutnik',
            'name' => 'ss_twitter',
            'type' => 'url',
            'instructions' => 'Celá adresa Twitter profilu ehutnik.cz. Pokud nebude vyplněno tak se nevypíše.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
        ),
        array(
            'key' => 'field_5a0ac5e4698ef',
            'label' => 'Facebook eHutnik',
            'name' => 'ss_facebook',
            'type' => 'url',
            'instructions' => 'Celá adresa Facebook profilu ehutnik.cz. Pokud nebude vyplněno tak se nevypíše.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
        ),
        array(
            'key' => 'field_5a0ac5f3698f0',
            'label' => 'Instragram eHutnik',
            'name' => 'ss_instagram',
            'type' => 'url',
            'instructions' => 'Celá adresa Instragramového profilu ehutnik.cz. Pokud nebude vyplněno tak se nevypíše.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
        ),
        array(
            'key' => 'field_5a0ac605698f1',
            'label' => 'Youtube eHutnik',
            'name' => 'ss_youtube',
            'type' => 'url',
            'instructions' => 'Celá adresa Youtube profilu ehutnik.cz. Pokud nebude vyplněno tak se nevypíše.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'ehutnik-general-settings',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5a1d2c1696241',
    'title' => 'Uživatel (redaktor)',
    'fields' => array(
        array(
            'key' => 'field_5a1d2cb0d1556',
            'label' => 'Foto redaktora',
            'name' => 'user_foto',
            'type' => 'image',
            'instructions' => 'Profilová fotka o rozměru 400x400 px',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumb-tiny',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => 500,
            'max_height' => 500,
            'max_size' => '',
            'mime_types' => 'JPG,PNG',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'user_form',
                'operator' => '==',
                'value' => 'all',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

acf_add_local_field_group(array(
    'key' => 'group_5a7b1c1bb8875',
    'title' => 'Autor příspěvku (vlastní)',
    'fields' => array(
        array(
            'key' => 'field_5a7b1c2e5c504',
            'label' => 'Jiný autor?',
            'name' => 'jiny_autor',
            'type' => 'true_false',
            'instructions' => 'Zaškrtněte pokud je článek publikován v zastoupení a chcete uvést jeho autora.',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'message' => 'Uvést jiného autora',
            'default_value' => 0,
            'ui' => 0,
            'ui_on_text' => '',
            'ui_off_text' => '',
        ),
        array(
            'key' => 'field_5a7b1c895c505',
            'label' => 'Jméno autora',
            'name' => 'jiny_autor_jmeno',
            'type' => 'text',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => 'field_5a7b1c2e5c504',
                        'operator' => '==',
                        'value' => '1',
                    ),
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;
