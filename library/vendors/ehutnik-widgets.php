<?php
/**
 * Homepage widgets for eHutnik.cz website
 *
 * @package eHutnik_1.0
 */

/**
 * Widget Get recent posts from a category
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 * source: https://gist.github.com/paulruescher/2998060
 */

Class eHutnik_Shorts_Widget extends WP_Widget {

 	// Contructor for Widget
    public function __construct()
    {
        parent::__construct(

 		// Base ID of our widget
 		'ehutnik_kratkezpravy',

 		// Widget name will appear in UI
 		__('eHutnik – Short news', 'ehutnik'),

 		// Widget description
 		array( 'description' => __('Short news from the region.', 'ehutnik'), )
 		);
    }

 	function widget($args, $instance) {

 		extract( $args );

 		$title = apply_filters('widget_title', empty($instance['title']) ? __('Short news from region') : $instance['title'], $instance, $this->id_base);

 		if( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) )
 			$number = 10;

 		//if( empty( $instance['categoryID'] ) || ! $categoryID = absint( $instance['categoryID'] ) )
 		//	$categoryID = 0;

 		$r = new WP_Query(
 			apply_filters( 'widget_posts_args',
 				array(  'posts_per_page' => $number,
 						'no_found_rows' => true,
 						'post_status' => 'publish',
 						'ignore_sticky_posts' => true,
 						//'cat' => $categoryID
 						'tax_query' => array(
        					array(
            					'taxonomy' => 'post_format',
            					'field' => 'slug',
            					'terms' => array( 'post-format-status' )
        					)
    					)
 				)
 			)
 		);
 		if( $r->have_posts() ) :

 			echo $before_widget;
 			if( $title ) echo $before_title . $title . $after_title; ?>
 			<ul>
 				<?php while( $r->have_posts() ) : $r->the_post(); ?>
				<?php
					// get tha tags
					$shorttags = wp_get_post_tags( $r->post->ID );

					if ( $shorttags ) {
						$tags = "";
						foreach ($shorttags as $tag) {
							//$tags .= '<a href="'.get_tag_link( $tag->term_id ).'">'.$tag->name.'</a>';
							$tags .= '<span class="tag">'.$tag->name.'</span> ';
						}
					} else {
						$tags = "";
					}
				?>
 				<li><div class="entry-meta"><?php the_time( 'd. m. Y H:i' ); ?> / <span class="tag"><?php echo $tags; ?></span></div> <?php the_content(); ?></li>
 				<?php endwhile; ?>
 			</ul>

 			<?php
 			echo $after_widget;

 			wp_reset_postdata();

 		endif;
 	}

 	// Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('Short news from region', 'ehutnik');
        }
 		if (isset($instance[ 'number' ])) {
        	$number = $instance[ 'number' ];
        } else {
            $number = 10;
         }
 		//if (isset($instance[ 'categoryID' ])) {
        //    $categoryID = $instance[ 'categoryID' ];
        //} else {
        //    $categoryID = 0;
        //}

         // Widget admin form ?>
 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
 		</p>
 		<p>
 			<label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="number" value="<?php echo esc_attr($number); ?>" />
 		</p>
		<!--
 		<p>
 			<label for="<?php /* echo $this->get_field_id('categoryID'); */ ?>"><?php /* _e('ID of category to show:'); */ ?></label>
 			<input class="widefat" id="<?php /* echo $this->get_field_id('categoryID'); */ ?>" name="<?php /* echo $this->get_field_name('categoryID'); */ ?>" type="number" value="<?php /* echo esc_attr($categoryID); */ ?>" />
 		</p>
		-->
 		<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
 		$instance['number'] = (! empty($new_instance['number'])) ? strip_tags($new_instance['number']) : '';
 		//$instance['categoryID'] = (! empty($new_instance['categoryID'])) ? strip_tags($new_instance['categoryID']) : '';
        return $instance;
    }
}
function ehutnik_wshorts() {
   register_widget('eHutnik_Shorts_Widget');
}
add_action('widgets_init', 'ehutnik_wshorts');


/**
 * Print tags from a category
 * source: https://stackoverflow.com/a/22788340
 * @param $atts array of configuration attributes
 *                    'categories'  ID of categories to show
 *                    'count' 		number of tags to show
 * @return String
 */

function ehutnik_get_category_tags($args) {
    global $wpdb;

 	$tags = [];

 	if ( empty( $args[''] ) ) {		// if we're not limited, show all tags of category
 		$max_tags = FALSE;
 	} else {
 		$max_tags = $args['count'];	// maximum number of tags to show
 	}

 	//https://codex.wordpress.org/Transients_API

 	$transcient_id = "Tags_Cloud_for_" . $args['categories'];
 	//delete_transient( $transcient_id );
 	if ( false === ( $tags = get_transient( $transcient_id ) ) ) {

     	$tags = $wpdb->get_results
 		("
         	SELECT DISTINCT
 				terms2.term_id as tag_id,
 				terms2.name as tag_name,
 				null as tag_link,
 				t2.count as posts_with_tag
         	FROM
             	$wpdb->posts as p1
             	LEFT JOIN $wpdb->term_relationships as r1 ON p1.ID = r1.object_ID
             	LEFT JOIN $wpdb->term_taxonomy as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
             	LEFT JOIN $wpdb->terms as terms1 ON t1.term_id = terms1.term_id,

             	$wpdb->posts as p2
             	LEFT JOIN $wpdb->term_relationships as r2 ON p2.ID = r2.object_ID
             	LEFT JOIN $wpdb->term_taxonomy as t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
             	LEFT JOIN $wpdb->terms as terms2 ON t2.term_id = terms2.term_id
         	WHERE
             	t1.taxonomy = 'category' AND
 				p1.post_status = 'publish' AND
 				terms1.term_id IN (".$args['categories'].") AND
             	t2.taxonomy = 'post_tag' AND
 				p2.post_status = 'publish' AND
 				p1.ID = p2.ID
     	");

 		//Cache tags for 12 hours
 		set_transient( $transcient_id, $tags, 12 * HOUR_IN_SECONDS );

 	}

    $count = 0;

 	// Create links for tags
    foreach ($tags as $tag) {
        $tags[$count]->tag_link = get_tag_link($tag->tag_id);
        $count++;
    }

 	// Sort tags by the number of posts in them
 	asort($tags);

 	// If we should crop the array to max number of tags then do so
 	if ( $max_tags ) {
 		$tags = array_slice( $tags, 0, $max_tags);
 	}

 	// Print/Return list of tags
 	$tags_result = '<ul class="category-tags">';
 	foreach ($tags as $tag) {
     	$tags_result .= "<li><a href=\"$tag->tag_link\" rel=\"tag\">$tag->tag_name</a></li>";
 	}
 	$tags_result .= '</ul>';

 	return $tags_result;
}

/**
 * Print recent posts from chosen category + the most used tags (for homepage)
 *
 * @param $atts array of configuration attributes
 *                    'categoryID'  ID of category to show
 *                    'count_posts'	number of posts to show
 *                    'count_tags' 	number of tags to show
 *                    'title'		title of block to show
 *                    'more_text'	text for "More" link
 *
  * @return String
 */

Class eHutnik_Homepage_Category_Posts_Widget extends WP_Widget {

 	// Contructor for Widget
    public function __construct()
    {
        parent::__construct(

 		// Base ID of our widget
 		'ehutnik_homepage_category_posts',

 		// Widget name will appear in UI
 		__('eHutnik – Homepage Category Posts – with tags', 'ehutnik'),

 		// Widget description
 		array( 'description' => __('Recent posts from a chosen category for homepage use', 'ehutnik'), )
 		);
    }

	private function category_tags($category, $numbertags) {
	    global $wpdb;

	 	$tags = [];

	 	if ( empty( $numbertags ) ) {	// if we're not limited, show all tags of category
	 		$max_tags = FALSE;
	 	} elseif ( $numbertags === -1 ) {
	 		$max_tags = 0;	// maximum number of tags to show
	 	} else {
	 		$max_tags = $numbertags;	// maximum number of tags to show
	 	}

	 	//https://codex.wordpress.org/Transients_API

	 	$transcient_id = "Tags_Cloud_for_" . $category;
	 	delete_transient( $transcient_id );
	 	if ( false === ( $tags = get_transient( $transcient_id ) ) ) {

	     	$tags = $wpdb->get_results
	 		("
	         	SELECT DISTINCT
	 				terms2.term_id as tag_id,
	 				terms2.name as tag_name,
	 				null as tag_link,
	 				t2.count as posts_with_tag
	         	FROM
	             	$wpdb->posts as p1
	             	LEFT JOIN $wpdb->term_relationships as r1 ON p1.ID = r1.object_ID
	             	LEFT JOIN $wpdb->term_taxonomy as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
	             	LEFT JOIN $wpdb->terms as terms1 ON t1.term_id = terms1.term_id,

	             	$wpdb->posts as p2
	             	LEFT JOIN $wpdb->term_relationships as r2 ON p2.ID = r2.object_ID
	             	LEFT JOIN $wpdb->term_taxonomy as t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
	             	LEFT JOIN $wpdb->terms as terms2 ON t2.term_id = terms2.term_id
	         	WHERE
	             	t1.taxonomy = 'category' AND
	 				p1.post_status = 'publish' AND
	 				terms1.term_id = $category AND
	             	t2.taxonomy = 'post_tag' AND
	 				p2.post_status = 'publish' AND
	 				p1.ID = p2.ID
	     	");

	 		//Cache tags for 12 hours
	 		set_transient( $transcient_id, $tags, 12 * HOUR_IN_SECONDS );

	 	}

	    $count = 0;

	 	// Create links for tags
	    foreach ($tags as $tag) {
	        $tags[$count]->tag_link = get_tag_link($tag->tag_id);
	        $count++;
	    }

	 	// Sort tags by the number of posts in them
	 	asort($tags);

	 	// If we should crop the array to max number of tags then do so
	 	if ( $max_tags ) {
	 		$tags = array_slice( $tags, 0, $max_tags);
	 	}

	 	// Print/Return list of tags
	 	$tags_result = '<ul class="category-tags">';
	 	foreach ($tags as $tag) {
	     	$tags_result .= "<li><a href=\"$tag->tag_link\" rel=\"tag\">$tag->tag_name</a></li>";
	 	}
	 	$tags_result .= '</ul>';

	 	return $tags_result;
	}

 	function widget($args, $instance) {

 		extract( $args );

 		$title = apply_filters('widget_title', empty($instance['title']) ? __('News') : $instance['title'], $instance, $this->id_base);
		//$categoryName = apply_filters('sanitize_text_field', $instance['categoryName']);
        $category = apply_filters('absint', $instance['category']);
		$count_posts = apply_filters('absint', $instance['count_posts']);
		$count_tags = apply_filters('absint', $instance['count_tags']);
		$more_text = apply_filters('sanitize_text_field', $instance['more_text']);
        $post_template = apply_filters('sanitize_text_field', $instance['post_template']);

		//$catID = get_cat_ID( $categoryName );
		$catslug = '';

        if ( $category == -1 ) {
            $catID = 0;
        } else {
            $catID = $category;
			$catInfo = get_term_by('id', $catID, 'category');
			$catslug = $catInfo->slug;
        }

        if ( empty($post_template) ) {
			// Standardní posty s obrázkem, template-parts/cat-default.php
            $post_template = 'default';
        }

		// Find whether we have and AD to display in text ads and if so then lower number of posts by 1
		$textova_inzerce = '';
		if ( $post_template === 'textonly' ) {
			if( function_exists('get_ad_placement') ) {
				$textova_inzerce = get_ad_placement('textova-inzerce');
				if ( $textova_inzerce ) {
					$count_posts = $count_posts - 1;
				}
			}
		}

 		$r = new WP_Query(
 			apply_filters( 'widget_posts_args',
 				array(  'posts_per_page' => $count_posts,
 						'no_found_rows' => true,
 						'post_status' => 'publish',
 						'ignore_sticky_posts' => true,
 						'cat' => $catID
 				)
 			)
 		);

 		if ( $r->have_posts() ) {

			if ( !$catslug ) {
				$before_widget = str_replace ( '_posts">', '_posts category-'.$catslug.'">', $before_widget );
			}
			echo $before_widget;

			echo '<header class="category-header">';
 			if ( $title ) {
				echo $before_title . $title . $after_title;
			}

			echo $this->category_tags($catID, $count_tags);
			echo '</header>';
			echo '<div class="news-container">';
			$post_counter = 0;

			while( $r->have_posts() ) : $r->the_post();
				if ( $post_counter === 0 ) {
					set_query_var( 'first_article', TRUE );
				} else {
					set_query_var( 'first_article', FALSE );
				}

				$post_counter++;

				if ( ($post_counter == 3) && ( $post_template === 'textonly' ) ) {
					echo $textova_inzerce;
				}

				get_template_part( 'template-parts/cat', $post_template );
			endwhile;

			$category_link = get_category_link( $catID );

			echo '</div> <!--#news-container -->';
			if ( sanitize_text_field($more_text) ) {
				echo '<footer class="category-footer">';
				echo '<a href="'. esc_url( $category_link ) .'" title="'.sanitize_text_field($more_text).'"  class="icon arrow-right--rounded">'.sanitize_text_field($more_text).'</a>';
				echo '</footer>';
			}

 			echo $after_widget;

		}

 		wp_reset_postdata();
 	}

 	// Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('Custom category title', 'ehutnik');
        }
 		if (isset($instance[ 'count_posts' ])) {
        	$count_posts = $instance[ 'count_posts' ];
        } else {
            $count_posts = 3;
        }
		if (isset($instance[ 'count_tags' ])) {
        	$count_tags = $instance[ 'count_tags' ];
        } else {
            $count_tags = 5;
        }
 		//if (isset($instance[ 'categoryName' ])) {
        //    $categoryName = $instance[ 'categoryName' ];
        //} else {
        //    $categoryName = 'General';
        //}
        if (isset($instance[ 'category' ])) {
            $category = $instance[ 'category' ];
        } else {
            $category = '';
        }
		if (isset($instance[ 'more_text' ])) {
            $more_text = $instance[ 'more_text' ];
        } else {
            $more_text = __('More news', 'ehutnik');
        }
		if (!empty($instance[ 'post_template' ])) {
            $post_template = $instance[ 'post_template' ];
        } else {
            $post_template = FALSE;
        }

        // Widget admin form ?>
 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
 		</p>
        <p>
            <label for="<?php echo $this->get_field_id( 'category' ); ?>"><?php _e( 'Select category' ); ?>:</label>
            <?php wp_dropdown_categories( array( 'show_option_none' =>' ','name' => $this->get_field_name( 'category' ), 'selected' => $category, 'class' => 'widefat' ) ); ?>
        </p>
 		<p>
 			<label for="<?php echo $this->get_field_id('count_posts'); ?>"><?php _e('Number of posts:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('count_posts'); ?>" name="<?php echo $this->get_field_name('count_posts'); ?>" type="number" value="<?php echo esc_attr($count_posts); ?>" />
 		</p>
		<p>
 			<label for="<?php echo $this->get_field_id('count_tags'); ?>"><?php _e('Number of tags (all: 0, none: -1):'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('count_tags'); ?>" name="<?php echo $this->get_field_name('count_tags'); ?>" type="number" value="<?php echo esc_attr($count_tags); ?>" />
 		</p>
		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('More news text:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('more_text'); ?>" name="<?php echo $this->get_field_name('more_text'); ?>" type="text" value="<?php echo esc_attr($more_text); ?>" />
 		</p>
        <p>
            <label for="<?php echo $this->get_field_id( 'post_template' ); ?>"><?php _e( 'Show posts as' ); ?>:</label>
            <select name="<?php echo $this->get_field_name( 'post_template' ); ?>" id="<?php echo $this->get_field_id( 'post_template' ); ?>" class="widefat" type="text">
	            <option class="level-0" value="-1" > </option>
				<option class="level-0" value="default" <?php selected( $post_template, 'default'); ?>>Standard</option>
	            <option class="level-0" value="textonly" <?php selected( $post_template, 'textonly'); ?>>Text only</option>
            </select>
        </p>
 		<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
 		//$instance['categoryName'] = (! empty($new_instance['categoryName'])) ? strip_tags($new_instance['categoryName']) : '';
        $instance['category'] = (! empty($new_instance['category'])) ? strip_tags($new_instance['category']) : '';
 		$instance['count_posts'] = (! empty($new_instance['count_posts'])) ? strip_tags($new_instance['count_posts']) : '';
 		$instance['count_tags'] = (! empty($new_instance['count_tags'])) ? strip_tags($new_instance['count_tags']) : '';
 		$instance['more_text'] = (! empty($new_instance['more_text'])) ? strip_tags($new_instance['more_text']) : '';
        $instance['post_template'] = (! empty($new_instance['post_template'])) ? strip_tags($new_instance['post_template']) : '';
        return $instance;
    }
}
function ehutnik_homepage_category_posts() {
   register_widget('eHutnik_Homepage_Category_Posts_Widget');
}
add_action('widgets_init', 'ehutnik_homepage_category_posts');




/**
 * Print recent posts from chosen category + the most used tags
 *
 * @param $atts array of configuration attributes
 *                    'categoryID'  ID of category to show
 *                    'count_posts'	number of posts to show
 *                    'count_tags' 	number of tags to show
 *                    'title'		title of block to show
 *                    'more_text'	text for "More" link
 *
  * @return String
 */

Class eHutnik_Recent_Category_Posts_Widget extends WP_Widget {

 	// Contructor for Widget
    public function __construct()
    {
        parent::__construct(

 		// Base ID of our widget
 		'ehutnik_recent_category_posts',

 		// Widget name will appear in UI
 		__('eHutnik – Recent Category Posts', 'ehutnik'),

 		// Widget description
 		array( 'description' => __('Recent posts from chosen categories', 'ehutnik'), )
 		);
    }

 	function widget($args, $instance) {

 		extract( $args );

 		$title = apply_filters('widget_title', empty($instance['title']) ? __('News') : $instance['title'], $instance, $this->id_base);
		//$categoryName = apply_filters('sanitize_text_field', $instance['categoryName']);
        $categories = $instance['categories'];
		$count_posts = apply_filters('absint', $instance['count_posts']);
		//$more_text = apply_filters('sanitize_text_field', $instance['more_text']);
        $post_template = apply_filters('sanitize_text_field', $instance['post_template']);

		$current_cat_placeholder = -999;

        if ( empty($post_template) ) {
			// Standardní posty s obrázkem, template-parts/cat-default.php
            $post_template = 'default';
        }

		$current_post = get_queried_object();
		$post_id = $current_post ? $current_post->ID : null;

		if ( !$categories ) {
            $catID = 0;
        } else {
			$catID = implode(',', $categories);

			// test if there is "current category" option selected and replace it with correct value in the string
			if ( in_array( $current_cat_placeholder, $categories) ) {
				$post_category = get_the_category( $post_id );
				if ( !empty($post_category) ) {
					$post_cat = $post_category[0]->cat_ID;
				} else {
					$post_cat = 0;
				}

				$catID = str_replace( $current_cat_placeholder, $post_cat, $catID );
			}
        }

 		$r = new WP_Query(
 			apply_filters( 'widget_posts_args',
 				array(  'posts_per_page' => $count_posts,
 						'no_found_rows' => true,
 						'post_status' => 'publish',
 						'ignore_sticky_posts' => true,
 						'cat' => $catID,
						'post__not_in' => array( $post_id )
 				)
 			)
 		);

 		if ( $r->have_posts() ) {
			echo $before_widget;

			echo '<header class="category-header">';
 			if ( $title ) {
				echo $before_title . $title . $after_title;
			}

			echo '</header>';
			echo '<div class="recent-news-container">';
			$post_counter = 0;

			while( $r->have_posts() ) : $r->the_post();
				set_query_var( 'first_article', FALSE );	// just to besure we have no featured styling
				get_template_part( 'template-parts/cat', $post_template );
			endwhile;

			echo '</div> <!--#recent-news-container -->';
 			echo $after_widget;

		}

 		wp_reset_postdata();
 	}

 	// Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('Title', 'ehutnik');
        }
 		if (isset($instance[ 'count_posts' ])) {
        	$count_posts = $instance[ 'count_posts' ];
        } else {
            $count_posts = 3;
        }
        if (isset($instance[ 'categories' ])) {
            $categories = $instance[ 'categories' ];
        } else {
            $categories = array();
        }
		if (!empty($instance[ 'post_template' ])) {
            $post_template = $instance[ 'post_template' ];
        } else {
            $post_template = FALSE;
        }

        // Widget admin form ?>
 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
 		</p>
        <p>
            <label for="<?php echo $this->get_field_id( 'categories' ); ?>"><?php _e( 'Select category' ); ?>:</label>
			<?php
			$args = array(
				'orderby' => 'name',
				'parent' => 0
		    );
		    $cat_list = get_categories( $args );
			if( $cat_list ) {
            	printf(
                	'<select multiple="multiple" name="%s[]" id="%s" class="widefat" size="15">',
                	$this->get_field_name('categories'),
                	$this->get_field_id('categories')
            	);
            	foreach( $cat_list as $cat )
            	{
                	printf(
                    	'<option value="%s" %s>%s</option>',
                    	$cat->term_id,
                    	in_array( $cat->cat_ID, $categories) ? 'selected="selected"' : '',
                    	$cat->cat_name
                	);
            	}
				printf(
					'<option value="-999" %s>Ze stejné kategorie</option>',
					in_array( -999, $categories) ? 'selected="selected"' : ''
				);
            	echo '</select>';
        	}
        	else
            	echo 'Found no categories yet';
			?>
        </p>
 		<p>
 			<label for="<?php echo $this->get_field_id('count_posts'); ?>"><?php _e('Number of posts:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('count_posts'); ?>" name="<?php echo $this->get_field_name('count_posts'); ?>" type="number" value="<?php echo esc_attr($count_posts); ?>" />
 		</p>
        <p>
            <label for="<?php echo $this->get_field_id( 'post_template' ); ?>"><?php _e( 'Show posts as' ); ?>:</label>
            <select name="<?php echo $this->get_field_name( 'post_template' ); ?>" id="<?php echo $this->get_field_id( 'post_template' ); ?>" class="widefat" type="text">
	            <option class="level-0" value="-1" > </option>
				<option class="level-0" value="default" <?php selected( $post_template, 'default'); ?>>Standard</option>
	            <option class="level-0" value="textonly" <?php selected( $post_template, 'textonly'); ?>>Text only</option>
            </select>
        </p>
 		<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['categories'] = (! empty($new_instance['categories'])) ? esc_sql($new_instance['categories']) : '';
 		$instance['count_posts'] = (! empty($new_instance['count_posts'])) ? strip_tags($new_instance['count_posts']) : '';
        $instance['post_template'] = (! empty($new_instance['post_template'])) ? strip_tags($new_instance['post_template']) : '';
        return $instance;
    }
}
function ehutnik_recent_category_posts() {
   register_widget('eHutnik_Recent_Category_Posts_Widget');
}
add_action('widgets_init', 'ehutnik_recent_category_posts');



/**
 * Print recent galleries from Obrazem
 *
 * @param $atts array of configuration attributes
 *                    'count_posts'	number of galleries to show
 *                    'title'		title of block to show
 *                    'more_text'	text for "More" link
 *
  * @return String
 */

Class eHutnik_InPictures_Widget extends WP_Widget {

 	// Contructor for Widget
    public function __construct()
    {
        parent::__construct(

 		// Base ID of our widget
 		'ehutnik_obrazem',

 		// Widget name will appear in UI
 		__('eHutnik – In Pictures', 'ehutnik'),

 		// Widget description
 		array( 'description' => __('Region in pictures. List latest galleries from "Obrazem"', 'ehutnik'), )
 		);
    }

 	function widget($args, $instance) {

 		extract( $args );

 		$title = apply_filters('widget_title', empty($instance['title']) ? __('News') : $instance['title'], $instance, $this->id_base);
		$count_posts = apply_filters('absint', $instance['count_posts']);
		$more_text = apply_filters('sanitize_text_field', $instance['more_text']);

 		$r = new WP_Query(
 			apply_filters( 'widget_posts_args',
 				array(  'posts_per_page' 		=> $count_posts,
 						'no_found_rows' 		=> true,
 						'post_status' 			=> 'publish',
 						'ignore_sticky_posts' 	=> true,
						//'post_type' 			=> 'obrazem'
						'tax_query' => array(
        					array(
            					'taxonomy' => 'post_format',
            					'field' => 'slug',
            					'terms' => array( 'post-format-gallery' )
        					)
    					)
 				)
 			)
 		);

 		if ( $r->have_posts() ) {

			//$before_widget = str_replace ( '_posts">', '_posts category-'.$catslug.'">', $before_widget );
			echo $before_widget;

			echo '<header class="gallery-header">';
 			if ( $title ) {
				echo $before_title . $title . $after_title;
			}

			echo '</header>';
			echo '<div class="gallery-container slider-obrazem">';

			while( $r->have_posts() ) : $r->the_post();
				get_template_part( 'template-parts/gallery-widget' );
			endwhile;

			echo '</div> <!--#gallery-container -->';
			//echo '<footer class="gallery-footer"><a href="'. esc_url( $category_link ) .'" title="'.$more_text.'"  class="icon arrow-right--rounded">'.$more_text.'</a></footer>';

 			echo $after_widget;

		}

 		wp_reset_postdata();
 	}

 	// Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('In pictures', 'ehutnik');
        }
 		if (isset($instance[ 'count_posts' ])) {
        	$count_posts = $instance[ 'count_posts' ];
        } else {
            $count_posts = 5;
        }
		if (isset($instance[ 'more_text' ])) {
            $more_text = $instance[ 'more_text' ];
        } else {
            $more_text = __('More galleries', 'ehutnik');
        }

        // Widget admin form ?>
 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
 		</p>
 		<p>
 			<label for="<?php echo $this->get_field_id('count_posts'); ?>"><?php _e('Number of galleries:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('count_posts'); ?>" name="<?php echo $this->get_field_name('count_posts'); ?>" type="number" value="<?php echo esc_attr($count_posts); ?>" />
 		</p>
		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('More galleries:'); ?></label>
 			<input class="widefat" id="<?php echo $this->get_field_id('more_text'); ?>" name="<?php echo $this->get_field_name('more_text'); ?>" type="text" value="<?php echo esc_attr($more_text); ?>" />
 		</p>
 		<?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
 		$instance['count_posts'] = (! empty($new_instance['count_posts'])) ? strip_tags($new_instance['count_posts']) : '';
 		$instance['more_text'] = (! empty($new_instance['more_text'])) ? strip_tags($new_instance['more_text']) : '';
        return $instance;
    }
}

function ehutnik_inpictures() {
   register_widget('eHutnik_InPictures_Widget');
}

add_action('widgets_init', 'ehutnik_inpictures');
