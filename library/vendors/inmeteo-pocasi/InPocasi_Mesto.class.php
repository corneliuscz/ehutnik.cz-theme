<?php
    /**
     * Pocasi na Web class (Mesto)
     * Modified to work as a part of custom Wordpress theme
     * Target theme: eHutnik.cz
     *
     *
     * @author In-počasí 2014
     *         Martin "Perry" Prantl
     * @author Josef Vybíral (graphic house s.r.o.)
     *
     * @version 1.1.2
     *
     */
    class InPocasi_Mesto
    {
        //=====================================================================================================
        //===== Constants for settings ========================================================================
        //=====================================================================================================

        //public static $LOCAL_XML_PATH = "./user-data/";

        //--------------------------------------------------------------------------------------------------
        //values below can be changed, but usually it is not needed
        //--------------------------------------------------------------------------------------------------

        //public static $USER_DATA_URL = "./user-data";    //URL of icon wind (if you don have your own, leave it as it is)

        //public static $ICON_SET_URL = "ikony1";    //URL of icon set (if you don have your own, leave it as it is)

        public static $GET_CHANGE_DAY_PARAMETER = "pocasi_den"; //parametr for URL to change days

        public static $WEEK_DAYS = array("Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"); //days in week in local language - starts with Sunday

        public static $DAY_TIME = array("Ráno", "Dopoledne", "Odpoledne", "Večer");    //type of daytime

        public static $WIND_DIRS = array(
                    0 => "Severní vítr",
                    1 => "Severovýchodní vítr",
                    2 => "Východní vítr",
                    3 => "Jihovýchodní vítr",
                    4 => "Jižní vítr",
                    5 => "Jihozápadní vítr",
                    6 => "Západní vítr",
                    7 => "Severozápadní vítr"
                    ); //convert ID of wind direction to its description

        public static $TEMP_CHANGE = array(0 => "klesá", 1 => "stoupá");



        //====================================================================================================
        //==== private variables =============================================================================
        //= do not modify code below this ====================================================================
        //====================================================================================================

        const XML_URL = "http://www.in-pocasi.cz/pocasi-na-web/xml.php?id=";

        private $xmlID;
        private $xmlDir;
        private $assetsURI;
        private $iconset;
        private $lastError;
        private $xmlOpenFile;
        private $xml;
        private $actualWeather;
        private $upperMenu;
        private $dayPrediction;
        private $activeCity;
        private $data;

        //======================================================================================================
        //=========== ctor =====================================================================================
        //======================================================================================================

        /**
         * ctor
         * @param string $xmlID
         *             ID of XML from www.in-pocasi.cz
         * @param string $xmlDIR
         *             Directory where to put downloaded XML data
         * @param string $assetsURI
         *             URI for assets
         * @param string $iconset
         *             Name of directory with weather icons
         *
         */
        public function __construct($xmlID, $xmlDir, $assetsURI, $iconset)
        {
            $this->xmlID = $xmlID;
            $this->xmlDir = $xmlDir.'/';
            $this->assetsURI = $assetsURI;
            $this->iconset = $iconset;

            $this->xml = NULL;
            $this->actualWeather = "";
            $this->upperMenu = "";
            $this->dayPrediction = "";
            $this->activeCity = "";
            $this->lastError = "";
            $this->xmlOpenFile = "";
            $this->data = array();
        }


        //=======================================================================================================
        //=========== Private method section ====================================================================
        //=======================================================================================================

        //=============== XML processing ========================================================================

        /**
         * Get file change in UNIX timestamp
         * @param string $filePath
         *             file path
         * @return int
         *             UNIX time of file change
         */
        private function GetCorrectMTime($filePath)
        {
            //http://php.net/manual/en/function.filectime.php
            $time = filectime($filePath);

            $isDST = (date('I', $time) == 1);
            $systemDST = (date('I') == 1);

            $adjustment = 0;

            if($isDST == false && $systemDST == true)
            {
                $adjustment = 3600;
            }

            else if($isDST == true && $systemDST == false)
            {
                $adjustment = -3600;
            }
            else
            {
                $adjustment = 0;
            }

            return ($time + $adjustment);
        }

        /**
         * Download XML from In-počasí server and store it on your local server
         * in directory specified in LOCAL_XML_PATH
         * @return boolean
         */
        private function DownloadXML()
        {
            if ($this->xmlDir == "")
            {
                $this->xmlOpenFile = InPocasi_Mesto::XML_URL.$this->xmlID;
                return true;
            }

            $tmp = explode("=", InPocasi_Mesto::XML_URL.$this->xmlID);
            $this->xmlOpenFile = $this->xmlDir;
            $this->xmlOpenFile .= $tmp[1];
            $this->xmlOpenFile .= ".xml";


            if (file_exists($this->xmlOpenFile))
            {
                //get last udate on the server
                $infoContent = $this->GetCorrectMTime($this->xmlOpenFile);
                if (abs($infoContent - time()) <= 60 * 60)
                {
                    //file is up to date
                    return true;
                }
            }



            $xmlContent = file_get_contents(InPocasi_Mesto::XML_URL.$this->xmlID); // your file is in the string "$xml" now.
            if (file_put_contents($this->xmlOpenFile, $xmlContent) === false)
            {
                return false;
            }



            return true;
        }

        /**
         * Load XML and parse it to find currently active city
         * and fill its data
         * Using SAX instead of DOM - we are only reading data in forward pass
         * No need to go back or skip something
         *
         * @return boolean
         *             - false = error occured in loading XML
         *             - true  = all is OK
         */
        private function LoadXML()
        {
            if ($this->DownloadXML() == false)
            {
                $this->lastError = "Nepodařilo se stáhnout informace o počasí.";
                return false;
            }

            $this->xml = new XmlReader();
            if ($this->xml->open($this->xmlOpenFile) == false)
            {
                $this->lastError = "Failed to read XML.";
                return false;
            }

            $this->data = array();

            $processed = false;
            while ($this->xml->read())
            {
                if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == "html")
                {
                    $this->lastError = "Nepodařilo se zpracovat data o počasí.";    // Nemáme správný feed, dostali jsme místo XML jen HTML stránku
                    return false;
                }

                if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == "mesto")
                {
                    if ($this->xml->getAttribute("id") == $this->activeCity)
                    {
                        $this->ProcessActiveCity();
                        $processed = true;

                        $this->data['mesto'] = $this->xml->getAttribute("id");
                    }
                }

                if ($processed)
                {
                    break;
                }

            }

            return true;
        }

        /**
         * Process single <mesto></mesto> from input XML
         */
        private function ProcessActiveCity()
        {
            $this->data['actual']['stav'] = 0;
            $this->data['actual']['teplota'] = 0;
            $this->data['actual']['teplota_zmena'] = 0;
            $this->data['actual']['vlhkost'] = 0;
            $this->data['actual']['vitr'] = 0;
            $this->data['actual']['vitr_smer'] = 0;
            $this->data['actual']['slunce_vychod'] = 0;
            $this->data['actual']['slunce_zapad'] = 0;

            while ($this->xml->read())
            {
                if ($this->xml->nodeType == XMLReader::END_ELEMENT && $this->xml->name == "mesto")
                {
                    break;
                }

                if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == "actual")
                {
                    $this->data['actual']['stav'] = $this->xml->getAttribute("stav");
                    $this->data['actual']['teplota'] = $this->xml->getAttribute("teplota");
                    $this->data['actual']['teplota_zmena'] = $this->xml->getAttribute("teplota_zmena");
                    $this->data['actual']['vlhkost'] = $this->xml->getAttribute("vlhkost");
                    $this->data['actual']['vitr'] = $this->xml->getAttribute("vitr");
                    $this->data['actual']['vitr_smer'] = $this->xml->getAttribute("vitr_smer");
                    $this->data['actual']['sunrise'] = $this->xml->getAttribute("slunce_vychod");
                    $this->data['actual']['sunset'] = $this->xml->getAttribute("slunce_zapad");


                    //check if current time is night
                    $this->data['actual']['is_night'] = 0;

                    $sunRise = strtotime($this->data['actual']['sunrise']) - strtotime('today');
                    $sunSet = strtotime($this->data['actual']['sunset']) - strtotime('today');
                    $act = strtotime('now') - strtotime('today');

                    if ($act < $sunRise) $this->data['actual']['is_night'] = 1;
                    if ($act > $sunSet) $this->data['actual']['is_night'] = 1;

                }

                if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == "den")
                {
                    $datum = $this->xml->getAttribute("datum");
                    $datumUnix = strtotime($datum);


                    $this->data[$datum] = $this->ProcessActiveDay();
                    $this->data[$datum]['datum_unix'] = $datumUnix;
                }

            }
        }

        /**
         * Process single <den></den> from input XML
         */
        private function ProcessActiveDay()
        {
            $dayData = array();
            $dayData['vitr']['global']['rychlost'] = 0;
            $dayData['vitr']['global']['smer'] = 0;
            $dayData['vitr']['rano']['rychlost'] = 0;
            $dayData['vitr']['rano']['smer'] = 0;
            $dayData['vitr']['dopoledne']['rychlost'] = 0;
            $dayData['vitr']['dopoledne']['smer'] = 0;
            $dayData['vitr']['odpoledne']['rychlost'] = 0;
            $dayData['vitr']['odpoledne']['smer'] = 0;
            $dayData['vitr']['noc']['rychlost'] = 0;
            $dayData['vitr']['noc']['smer'] = 0;

            $dayData['stav']['global'] = 0;
            $dayData['stav']['rano'] = 0;
            $dayData['stav']['dopoledne'] = 0;
            $dayData['stav']['odpoledne'] = 0;
            $dayData['stav']['noc'] = 0;

            $dayData['teplota']['global']['den'] = 0;
            $dayData['teplota']['global']['noc'] = 0;
            $dayData['teplota']['rano'] = 0;
            $dayData['teplota']['dopoledne'] = 0;
            $dayData['teplota']['odpoledne'] = 0;
            $dayData['teplota']['noc'] = 0;

            while ($this->xml->read())
            {
                if ($this->xml->nodeType == XMLReader::END_ELEMENT && $this->xml->name == "den")
                {
                    break;
                }

                if ($this->xml->nodeType == XMLReader::ELEMENT)
                {

                    if (isset($dayData[$this->xml->name]) == false)
                    {
                        continue;
                    }

                    $atr = $this->xml->getAttribute("id");
                    if (is_null($atr) == false)
                    {
                        //found attribute "id"
                        if ($this->xml->name == "vitr")
                        {
                            $dayData[$this->xml->name][$atr]['rychlost'] = $this->xml->getAttribute("rychlost");
                            $dayData[$this->xml->name][$atr]['smer'] = $this->xml->getAttribute("smer");
                        }
                        else
                        {
                            $dayData[$this->xml->name][$atr] = $this->xml->readString();

                            if (($this->xml->name == "stav") && ($atr == "noc"))
                            {
                                $dayData[$this->xml->name][$atr] .= "-noc";
                            }

                        }
                        continue;
                    }

                    $atr = $this->xml->getAttribute("den");
                    if (is_null($atr) == false)
                    {
                        $dayData[$this->xml->name]['global']['den'] = $atr;
                    }

                    $atr = $this->xml->getAttribute("noc");
                    if (is_null($atr) == false)
                    {
                        $dayData[$this->xml->name]['global']['noc'] = $atr;
                    }

                    $atr = $this->xml->getAttribute("rychlost");
                    if (is_null($atr) == false)
                    {
                        $dayData[$this->xml->name]['global']['rychlost'] = $atr;
                    }

                    $atr = $this->xml->getAttribute("smer");
                    if (is_null($atr) == false)
                    {
                        $dayData[$this->xml->name]['global']['smer'] = $atr;
                    }

                    if (($this->xml->name == 'stav') && ($this->xml->hasAttributes == false))
                    {
                        $dayData[$this->xml->name]['global'] = $this->xml->readString();
                    }


                }
            }


            return $dayData;
        }

        //===============================================================================================================

        /**
         * Get data based on tiem at format "Y-m-d"
         *
         * @param $time
         *             - "Y-m-d" time
         *
         * @return data for single day
         */
        private function GetDataForDate($date)
        {
            return $this->data[$date];
        }

        /**
         * Get data based on UNIX timestamp
         * Only "Y-m-d" is taken from timestamp
         *
         * @param $time
         *             - UNIX timestamp
         *
         * @return data for single day
         */
        private function GetDataForUNIXTime($time)
        {
            $date = date("Y-m-d", $time);

            return $this->GetDataForDate($date);
        }

        /**
         * get active day from URL $_GET parametr
         * @return number
         *             ID of active day
         */
        private function GetActiveDay()
        {
            $activeDay = 0;
            if (isset($_GET[InPocasi_Mesto::$GET_CHANGE_DAY_PARAMETER]))
            {
                $activeDay = intval($_GET[InPocasi_Mesto::$GET_CHANGE_DAY_PARAMETER]);
            }

            return $activeDay;
        }

        /**
         * Found currently active day (according to InPocasi_Mesto::GET_CHANGE_DAY_PARAMETER in URL)
         * and return data for it
         *
         * @return data for single day
         */
        private function GetActiveDayData()
        {
            $activeDay = $this->GetActiveDay();
            $i = 0;
            $foundData = array();

            foreach ($this->data as $key => $dayData)
            {
                if (($key == "actual") || ($key == "mesto"))
                {
                    continue;
                }

                if ($i == $activeDay)
                {
                    $foundData = $dayData;
                    break;
                }

                $i++;
            }

            return $foundData;
        }

        /**
         * Get current URL and return it without
         * InPocasi_Mesto::GET_CHANGE_DAY_PARAMETER parametr
         *
         * @param [out] $delim
         *                     - InPocasi_Mesto::GET_CHANGE_DAY_PARAMETER will be appended with ? or &
         * @return string
         *                 - URL
         *
         */
        private function GetActualURL(&$delim)
        {
            $actualLink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $pos = strpos($actualLink, "?");
            if ($pos !== false)
            {
                $actualLink = mb_substr($actualLink, 0, $pos);
            }

            $delim = '?';
            $i = 0;
            foreach ($_GET as $key => $value)
            {
                if ($key == InPocasi_Mesto::$GET_CHANGE_DAY_PARAMETER) continue;
                if ($i == 0)
                {
                    $actualLink .= '?';
                    $delim = '&';
                }
                else
                {
                    $actualLink .= '&';
                    $delim = '&';
                }

                $actualLink .= ''.$key.'='.$value;

                $i++;
            }

            return $actualLink;
        }

        /**
         * Generate HTML for actual part of the data
         */
        private function GenerateActual()
        {


            $this->actualWeather = "";

            $temp = $this->data['actual']['teplota'];
            $tempChange =  $this->data['actual']['teplota_zmena'];
            $windSpeed =  $this->data['actual']['vitr'];
            $windDir =  $this->data['actual']['vitr_smer'];
            $humid = $this->data['actual']['vlhkost'];
            $iconName = $this->data['actual']['stav'];
            $sunrise = $this->data['actual']['sunrise'];
            $sunset = $this->data['actual']['sunset'];


            $this->actualWeather .= '<h2 class="inpocasi_nadpis">'.$this->data['mesto'].' aktuálně</h2>';
            $this->actualWeather .= '<table>';
            $this->actualWeather .= '<tr>';
            if ($iconName == "-")
            {
                $this->actualWeather .= '<td></td>';
            }
            else
            {
                if ($this->data['actual']['is_night'])
                {
                    $iconName .= '-noc';
                }
                $this->actualWeather .= '<td><img src="'.$this->assetsURI.'/'.$this->iconset.'/'.$iconName.'.png" alt="Počasí"> </td>';
            }
            $this->actualWeather .= '<td>';
            $this->actualWeather .= 'Teplota: <span>'.$temp.' °C</span> ('.InPocasi_Mesto::$TEMP_CHANGE[$tempChange].')<br />';
            $this->actualWeather .= 'Vlhkost: <span>'.$humid.'%</span><br />';
            $this->actualWeather .= 'Vítr: <span>'.$windSpeed.' km/h</span> <img src="'.$this->assetsURI.'/vitr/'.$windDir.'.png">';
            $this->actualWeather .= '</td>';
            $this->actualWeather .= '<td>';
            $this->actualWeather .= 'Východ slunce: <span>'.$sunrise.'</span><br />';
            $this->actualWeather .= 'Západ slunce: <span>'.$sunset.'</span> <br />';
            $this->actualWeather .= '</td>';
            $this->actualWeather .= '</tr>';
            $this->actualWeather .= '</table>';
            $this->actualWeather .= '<br />';
        }

        /**
         * Generate HTML for upper menu
         */
        private function GenerateUpperMenu()
        {
            $delim = '';
            $actualLink = $this->GetActualURL($delim);


            $activeDay = $this->GetActiveDay();
            $i = 0;

            $this->upperMenu = '<div class="inpocasi_menu">';
            foreach ($this->data as $key => $dayData)
            {
                if (($key == "actual") || ($key == "mesto"))
                {
                    continue;
                }

                $active = '';
                if ($i == $activeDay)
                {
                    $active = 'class="inaktivni"';
                }


                $dayID = date("w", $dayData['datum_unix']);
                $temperature = $dayData['teplota']['global']['den'];
                $iconName = $dayData['stav']['global'];

                $this->upperMenu .= '<a href="'.$actualLink.$delim.InPocasi_Mesto::$GET_CHANGE_DAY_PARAMETER.'='.$i.'" '.$active.'>';
                $this->upperMenu .= '<b>'.InPocasi_Mesto::$WEEK_DAYS[$dayID].'</b>';
                $this->upperMenu .= '<br />';
                $this->upperMenu .= '<img src="'.$this->assetsURI.'/'.$this->iconset.'/'.$iconName.'.png" width="40">';
                $this->upperMenu .= '<span>'.$temperature.' °C</span>';
                $this->upperMenu .= '</a>';

                $i++;
            }
            $this->upperMenu .= '</div>';
        }

        /**
         * Generate HTML for single day data
         */
        private function GenerateDayPrediction()
        {

            $this->dayPrediction = "";

            $foundData = $this->GetActiveDayData();
            $type = array("rano", "dopoledne", "odpoledne", "noc");

            for ($i = 0; $i < 4; $i++)
            {

                $temperature = $foundData['teplota'][$type[$i]];
                $iconName = $foundData['stav'][$type[$i]];

                $windSpeed =  $foundData['vitr'][$type[$i]]['rychlost'];
                $windDir =  InPocasi_Mesto::$WIND_DIRS[$foundData['vitr'][$type[$i]]['smer']];

                $bgColor = ($temperature > -10) ? ($temperature > 0) ? ($temperature > 10) ? ($temperature > 20) ? ($temperature > 30) ? "#EA0B02" : "#FF6905" : "#DAA505" : "#248E4E" : "#215679" : "#D205DA";

                $this->dayPrediction .= '<div class="inden">';
                  $this->dayPrediction .= '<b>'.InPocasi_Mesto::$DAY_TIME[$i].'</b>';
                  $this->dayPrediction .= '<br />';
                  $this->dayPrediction .= '<img src="'.$this->assetsURI.'/'.$this->iconset.'/'.$iconName.'.png">';
                  $this->dayPrediction .= '<br />';
                  $this->dayPrediction .= '<span style="background-color: '.$bgColor.';">'.$temperature.' °C</span>';
                  $this->dayPrediction .= '<br />';
                  $this->dayPrediction .= ''.$windDir.'';
                  $this->dayPrediction .= '<br />';
                  $this->dayPrediction .= ''.$windSpeed.' km/h';
                $this->dayPrediction .= '</div>';
            }

        }

        private function ToLower($text)
        {
            $zmena_velka = array("Á"=>"á", "Ä"=> "ä", "Č"=>"č", "Ď"=>"ď", "É"=>"é", "Ě"=>"ě","Š"=>"š", "Č"=>"č", "Ř"=>"ř", "Ž"=>"ž", "Ý"=>"ý", "Í"=>"í", "Ť"=>"ť", "Ň"=>"ň", "Ů"=>"ů", "Ú"=>"ú");
            $text = strtr($text, $zmena_velka);

            return strtolower($text);
        }

        //======================================================================================================
        //=========== Public method section ====================================================================
        //======================================================================================================

        /**
         * Get last error
         *
         * @return string
         */
        public function GetLastError()
        {
            return $this->lastError;
        }

        /**
         * Generate data
         *
         * This method must be called before we can proceed further
         * It will load data from XML for city with $cityID
         *
         * @param $cityID
         *             - ID of city from XML
         * @return boolean
         *              true if all is OK, false if generate failed
         */
        public function Generate($cityID)
        {
            if (trim($cityID) == '')
            {
                return false;
            }

            $this->activeCity = $cityID;

            if ($this->LoadXML() == false)
            {
                //Failed to load xml

                return false;
            }

            //print_r($this->data);

            $this->GenerateActual();
            $this->GenerateUpperMenu();
            $this->GenerateDayPrediction();

            return true;
        }

        /**
         * Get data for generated city
         * This can be used to store data in DB or elsewhere
         * after they have been loaded from XML
         */
        public function GetData()
        {
            return $this->data;
        }

        /**
         * Print loaded content directly to your screen
         */
        public function PrintContent()
        {

            $dayData = $this->GetActiveDayData();

            $datum = date("d. m. Y", $dayData['datum_unix']);
            $day = InPocasi_Mesto::$WEEK_DAYS[date("w", $dayData['datum_unix'])];
            $day = $this->ToLower($day);

            echo '<div class="inpocasi">';

            echo $this->actualWeather;

            echo $this->upperMenu;

            echo '<h2 class="inpocasi_nadpis">Předpověď počasí - '.$day.', '.$datum.'</h2>';
            echo '<br />';

            echo $this->dayPrediction;

            echo '<br clear="left">';
            echo '</div>';
        }


// ============== My own functions ============== //

        /**
         * Print content as banner for widget. Result can be as strip or bar,
         * depends on css style
         * @param $daysCount
         *             number of days in range <1, 5>
         * @param $iswidget
         *            True or False if we're showing a widget or shortcode
         */
        public function PrintWidgetBanner($daysCount, $iswidget = FALSE)
        {
            $daysCount = ($daysCount < 1) ? 1 : $daysCount;
            $daysCount = ($daysCount > 5) ? 5 : $daysCount;

            $count = 0;
            $den = '';
            $result = '';

            if ($iswidget) {
                $result = '<ul class="ehutnik_pocasi_pruh">';
            }
            foreach ($this->data as $key => $dayData)
            {
                if (($key == "actual") || ($key == "mesto")) {
                    continue;
                }

                $dayID = date("w", $dayData['datum_unix']);
                $temperature = $dayData['teplota']['global']['den'];
                $temperatureNight = $dayData['teplota']['global']['noc'];
                $iconName = $dayData['stav']['global'];

                $icon = "";
                if ($iconName == "-") {
                    $icon = '';
                }
                else {
                    $icon = '<img src="'.$this->assetsURI.'/'.$this->iconset.'/'.$iconName.'.png" alt="'.$iconName.'">';
                }

                if ($count == 0) {
                    $den = __('Dnes', 'ehutnik');
                } elseif ($count == 1) {
                    $den = __('Zítra', 'ehutnik');
                } elseif ($count == 2) {
                    $den = __('Pozítří', 'ehutnik');
                } else {
                    $den = InPocasi_Mesto::$WEEK_DAYS[$dayID];
                }

                $result .= '<li><span class="pocasi-den">'.$den.'</span><span class="pocasi-ikona_teplota">'.$icon.' <span class="pocasi-teplota">'.$temperature.' &deg;C</span></span></li>';

                $count++;
                if ($count >= $daysCount)
                {
                    break;
                }
            }
            if ($iswidget) {
                $result .= '</ul>';
            }

            return $result;
        }


    }
?>
