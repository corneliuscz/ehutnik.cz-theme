<?php
/**
 * Pocasi na Web – function/shortcode/widget whatever
 * Modified script from in-pocasi.cz to display weather in custom Wordpress installation
 * Target theme: eHutnik.cz
 *
 *
 * @author In-počasí 2014
 *         Martin "Perry" Prantl
 * @author Josef Vybíral (graphic house s.r.o.)
 *
 * @version 1.1.2
 *
 */

$InPocasiID     = 'put_it_in_widget';    // Put your in-pocasi.cz ID here
//$InPocasiID     = 'e58dca4cdc';    // Put your in-pocasi.cz ID here
$InPocasiCity    = 'Třinec';    // Default city to display weather for (if not set in shortcode)
$InPocasiXMLDir    = 'weather_data';    //LOCAL_XML_PATH, name of directory under 'uploads'
$InPocasiAssets    = 'assets/img/pocasi';    //USER_DATA_URL, where in assets can we find all icons
$InPocasiIconset    = 'ikony';    //ICON_SET_URL, which iconset under AssetsURI dir should we use

// Get some initial info for Class constructor
$upload = wp_upload_dir();
$upload_dir = $upload['basedir'];
$upload_dir = $upload_dir . '/' . $InPocasiXMLDir;

$assets = get_stylesheet_directory_uri();
$assets_uri = $assets . '/' . $InPocasiAssets;

// Setup the class
require_once get_stylesheet_directory() . '/library/vendors/inmeteo-pocasi/InPocasi_Mesto.class.php';

/**
 * Create necessary folders for the weather to work.
 * Creates 'weather_data' folder in 'uploads' folder
 */
function ehutnikWeather_activate() {
    global $InPocasiXMLDir;

    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/' . $InPocasiXMLDir;
    if (! is_dir($upload_dir)) {
       mkdir( $upload_dir, 0700 );
    }
}

add_action("after_switch_theme", "ehutnikWeather_activate");

/**
 * Shortcode to create bar of three days' weather
 * @param  string $city    City to display weather for (incl. diacritics)
 * @return string $weather    Formatted weather data
 */

function ehutnik_WeatherBar($params) {
    global $InPocasiID;
    global $InPocasiCity;
    global $InPocasiIconset;
    global $upload_dir;
    global $assets_uri;

    $atts = shortcode_atts(array(
        'city' => $InPocasiCity
    ), $params);

    $weather = "";

    $t = new InPocasi_Mesto($InPocasiID, $upload_dir, $assets_uri, $InPocasiIconset);
    $t->Generate($atts['city']);
    $error = $t->GetLastError();

    //$t->PrintContent();

    if ($error) {
        $weather = $error;
    } else {
        $weather = $t->PrintWidgetBanner(3);
    }

    return $weather;
}

add_shortcode('ehutnik_pocasi', 'ehutnik_WeatherBar');


/**
 * Widget Pocasi_ehutnik
 * @return void
 */
function ehutnik_widget_pocasi()
{
    register_widget('ehutnik_wpocasi');
}
add_action('widgets_init', 'ehutnik_widget_pocasi');

// Creating the widget
class ehutnik_wpocasi extends WP_Widget
{
    // Contructor for Widget
    public function __construct()
    {
        parent::__construct(

        // Base ID of our widget
        'ehutnik_pocasi',

        // Widget name will appear in UI
        __('Weather bar (in-pocasi.cz)', 'ehutnik'),

        // Widget description
        array( 'description' => __('Prints simple three day weather prediction for Třinec (default)', 'ehutnik'), )
        );
    }

    // Creating widget front-end
    public function widget($args, $instance)
    {
        global $InPocasiID;
        global $InPocasiCity;
        global $InPocasiIconset;
        global $upload_dir;
        global $assets_uri;

        $title = apply_filters('widget_title', $instance['title']);
        $pocasiID = apply_filters('sanitize_text_field', $instance['pocasiID']);
        $city_name = apply_filters('sanitize_text_field', $instance['city_name']);
        $number_of = apply_filters('absint', $instance['number_of']);

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (! empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        // Which city to display weather for
        if (empty($city_name)) {
            $city_name = $InPocasiCity;
        }

        // ID of InPocasi.cz subscription
        if (empty($pocasiID)) {
            $pocasiID = $InPocasiID;
        }

        $weather = "";

        $t = new InPocasi_Mesto($pocasiID, $upload_dir, $assets_uri, $InPocasiIconset);
        $t->Generate($city_name);
        $error = $t->GetLastError();

        if ($error) {
            $weather = '<div class="pocasi-error">'. $error .'</div>';
        } else {
            $weather = $t->PrintWidgetBanner($number_of, TRUE);
        }
        echo $weather;
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance)
    {
        if (isset($instance[ 'title' ])) {
            $title = $instance[ 'title' ];
        } else {
            $title = __('Počasí', 'ehutnik');
        }
        if (isset($instance[ 'pocasiID' ])) {
            $pocasiID = $instance[ 'pocasiID' ];
        } else {
            $pocasiID = '';
        }
        if (isset($instance[ 'city_name' ])) {
            $city_name = $instance[ 'city_name' ];
        } else {
            $city_name = __('Třinec', 'ehutnik');
        }
        if (isset($instance[ 'number_of' ])) {
            $number_of = $instance[ 'number_of' ];
        } else {
            $number_of = 3;
        }
        // Widget admin form ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'ehutnik'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('pocasiID'); ?>"><?php _e('InPočasí.cz ID předplatného (kód):', 'ehutnik'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('pocasiID'); ?>" name="<?php echo $this->get_field_name('pocasiID'); ?>" type="text" value="<?php echo esc_attr($pocasiID); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('city_name'); ?>"><?php _e('City:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('city_name'); ?>" name="<?php echo $this->get_field_name('city_name'); ?>" type="text" value="<?php echo esc_attr($city_name); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number_of'); ?>"><?php _e('Number of days of forecast (max 5, default 3):', 'ehutnik'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_of'); ?>" name="<?php echo $this->get_field_name('number_of'); ?>" type="text" value="<?php echo esc_attr($number_of); ?>" />
        </p>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (! empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['pocasiID'] = (! empty($new_instance['pocasiID'])) ? strip_tags($new_instance['pocasiID']) : '';
        $instance['city_name'] = (! empty($new_instance['city_name'])) ? strip_tags($new_instance['city_name']) : '';
        $instance['number_of'] = (! empty($new_instance['number_of'])) ? strip_tags($new_instance['number_of']) : '';
        return $instance;
    }
} // Class ehutnik_wpocasi ends here
