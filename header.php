<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eHutnik_1.0
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#9c0f12">
    <meta name="msapplication-TileColor" content="#9c0f12">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#9c0f12">

    <?php wp_head(); ?>

    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body <?php body_class(); ?>>
<!-- START Display Upgrade Message for IE 10 or Less -->
<!--[if lte IE 9]>
    <div style="background: #ea0000; text-align: center; position: relative; padding: 1rem 0; width: 100%; color: #FFF;">Používáte zastaralou verzi internetového prohlížeče, váš pohyb po internetu nemusí být bezpečný (a náš web nemusí fungovat správně)! <br> <a href="https://support.microsoft.com/cs-cz/help/17621/internet-explorer-downloads" target="_blank" style="color: #fff; text-decoration: underline;">Aktualizujte si prosím prohlížeč</a> nebo si <a href="https://browsehappy.com/" target="_blank" style="color: #fff; text-decoration: underline;">vyberte jiný</a></div>
<![endif]-->
<script>
// IF THE BROWSER IS INTERNET EXPLORER 10
if (navigator.appVersion.indexOf("MSIE 10") !== -1) {
    document.write("<div style=\"background: #ea0000; text-align: center; position: relative; padding: 1rem 0; width: 100%; color: #FFF;\">Používáte zastaralou verzi internetového prohlížeče, váš pohyb po internetu nemusí být bezpečný (a náš web nemusí fungovat správně)!<br><a href=\"https://support.microsoft.com/cs-cz/help/17621/internet-explorer-downloads\" target=\"_blank\" style=\"color: #fff; text-decoration: underline;\">Aktualizujte si prosím prohlížeč</a> nebo si <a href=\"https://browsehappy.com/\" target=\"_blank\" style=\"color: #fff; text-decoration: underline;\">vyberte jiný</a></div>");
}
</script>
<!-- END Display Upgrade Message for IE 10 or Less -->

<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ehutnik' ); ?></a>
    <?php get_sidebar('toplinks') ?>
    <header id="masthead" class="site-header">
        <div class="site-branding">
            <?php the_custom_logo(); ?>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><h1 class="site-title"><?php bloginfo( 'name' ); ?></h1></a>
            <p class="site-description"><?php bloginfo( 'description' ); ?></p>
        </div><!-- .site-branding -->

        <nav id="site-navigation" class="main-navigation">
            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span></span><?php esc_html_e( 'Menu', 'ehutnik' ); ?></button>
            <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                ) );
            ?>
        </nav><!-- #site-navigation -->

        <div id="site-search" class="header-searchbox">
        <?php
            get_search_form();
        ?>
        </div>
    </header><!-- #masthead -->

    <div id="content" class="site-content">
