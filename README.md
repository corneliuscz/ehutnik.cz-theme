[![Build Status](https://travis-ci.org/Automattic/_s.svg?branch=master)](https://travis-ci.org/Automattic/_s)

eHutnik.cz Wordpress template
=============================

Based on a starter theme called `_s`, or `underscores`. Grid based on `susy` by oddbird.

Pull it and run:
`npm install`
`bower install`

and pray and run `gulp`so it watches your files and compiles them
