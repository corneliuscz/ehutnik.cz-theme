<?php
/**
 * The sidebar containing the main widget area pro Single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eHutnik_1.0
 */

if ( ! is_active_sidebar( 'sidebar-foobar' ) ) {
	return;
}
?>

<!--<aside id="secondary" class="widget-area">-->
	<?php dynamic_sidebar( 'sidebar-foobar' ); ?>
<!--</aside><!-- #secondary -->
