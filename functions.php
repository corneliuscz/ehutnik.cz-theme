<?php
/**
 * eHutnik 1.0 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package eHutnik_1.0
 */

if ( ! function_exists( 'ehutnik_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function ehutnik_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on eHutnik 1.0, use a find and replace
         * to change 'ehutnik' to the name of your theme in all the template files.
         */
        load_theme_textdomain( 'ehutnik', get_template_directory() . '/library/languages' );

        // Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );

        add_image_size( 'thumb-tiny', 128, 128, true );              // Mobilní obrázek 64x64 (+ retina)
        add_image_size( 'thumb-medium-small', 650, 0, false );      // Střední velikost pro 480-768 px
        //add_image_size( 'news-thumb-medium', 340, 240, true );  // Náhled novinky v seznamu
        //add_image_size( 'news-thumb-medium', 960, 0, false );   // Náhled hlavní novinky

        //if ( false === get_option("medium_crop") ) {
        //    add_option( "medium_crop", "1" );
        //} else {
        //    update_option( "medium_crop", "1" );
        //}

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'menu-1' => esc_html__( 'Main menu', 'ehutnik' ),
            'menu-footer' => esc_html__( 'Footer menu', 'ehutnik' ),
        ) );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support( 'html5', array(
            'search-form',
//            'comment-form',
//            'comment-list',
            'gallery',
            'caption',
        ) );

        // Set up the WordPress core custom background feature.
        /*
        add_theme_support( 'custom-background', apply_filters( 'ehutnik_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        ) ) );
        */

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support( 'post-formats', array(
//          'aside',
//          'image',
//          'video',
//          'quote',
//          'link',
            'gallery',
//          'audio',
            'status'
        ) );

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        /*
        add_theme_support( 'custom-logo', array(
            'height'      => 250,
            'width'       => 250,
            'flex-width'  => true,
            'flex-height' => true,
        ) );
        */
    }
endif;
add_action( 'after_setup_theme', 'ehutnik_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ehutnik_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'ehutnik_content_width', 640 );
}
add_action( 'after_setup_theme', 'ehutnik_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ehutnik_widgets_init() {
    register_sidebar(
        array(
            'name'          => esc_html__( 'Homepage - bloky', 'ehutnik' ),
            'id'            => 'sidebar-hpcats',
            'description'   => esc_html__( 'Add Categories for Homepage here.', 'ehutnik' ),
            'before_widget' => '<section id="%1$s" class="hp-section %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="section-title">',
            'after_title'   => '</h2>',
            ));
    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar Top Right (homepage)', 'ehutnik' ),
            'id'            => 'sidebar-topinfo',
            'description'   => esc_html__( 'Add widgets like weather, namedays and ads here. It will be shown at the top right position of the page', 'ehutnik' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
            ));
    register_sidebar(
        array(
            'name'          => esc_html__( 'Sidebar (general)', 'ehutnik' ),
            'id'            => 'sidebar-1',
            'description'   => esc_html__( 'Add widgets here.', 'ehutnik' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
            ));
    register_sidebar(
        array(
            'name'          => esc_html__( 'Horní pruh pro odkazy', 'ehutnik' ),
            'id'            => 'sidebar-toplinks',
            'description'   => esc_html__( 'Here comes widget with links', 'ehutnik' ),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h6 class="screen-reader-text">',
            'after_title'   => '</h6>',
            ));
    register_sidebar(
        array(
            'name'          => esc_html__( 'Footer (single + archive)', 'ehutnik' ),
            'id'            => 'sidebar-foobar',
            'description'   => esc_html__( 'Additional footer section for single post and archives', 'ehutnik' ),
            'before_widget' => '<section id="%1$s" class="fullw foobar %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
            ));
}
add_action( 'widgets_init', 'ehutnik_widgets_init' );

/**
 * Enqueue styles.
 */
if ( ! function_exists( 'ehutnik_styles' ) ) :

    function ehutnik_styles() {
        if ( SCRIPT_DEBUG || WP_DEBUG ) :
            wp_register_style(
                'ehutnik-style', // handle name
                get_template_directory_uri() . '/assets/css/style.css?v=20190604', '', '1.5', 'screen'
            );
            wp_enqueue_style( 'ehutnik-style' );

            else :
            wp_register_style(
                'ehutnik-style', // handle name
                get_template_directory_uri() . '/assets/css/style-min.css?v=20190604', '', '1.5', 'screen'
            );
            wp_enqueue_style( 'ehutnik-style' );
        endif;
    }
  add_action( 'wp_enqueue_scripts', 'ehutnik_styles' );

endif; // Enqueue styles

/**
 * Deregister unnecessary styles
 */

function ehutnik_deregister_styles() {
    // Cookie notice
    wp_dequeue_style( 'cookie-notice-front' );
    wp_deregister_style( 'cookie-notice-front' );

    // Advanced CF7 DB
    wp_dequeue_style( 'advanced-cf7-db' );
    wp_deregister_style( 'advanced-cf7-db' );
}

add_action( 'wp_enqueue_scripts', 'ehutnik_deregister_styles', 100 );

/**
 * Enqueue scripts
 */
function ehutnik_scripts() {

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( SCRIPT_DEBUG || WP_DEBUG ) :
        // Concatonated Scripts
        wp_enqueue_script( 'ehutnik-js', get_stylesheet_directory_uri() . '/assets/js/development.js?v=20190326', array( 'jquery' ), '1.2.0', true );
    else :
        // Concatonated and minified Scripts
        wp_enqueue_script( 'ehutnik-js', get_stylesheet_directory_uri() . '/assets/js/production-min.js?v=20190326', array( 'jquery' ), '1.2.0', true );
    endif;
}
add_action( 'wp_enqueue_scripts', 'ehutnik_scripts' );

/**
 * Change login logo, url, title
 *
*/
function ehutnik_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ehutnik-pozitiv.png);
            height:36px;
            width:178px;
            background-size: 178px 36px;
            background-repeat: no-repeat;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'ehutnik_login_logo' );

function ehutnik_login_logourl() {
    return home_url();
}
add_filter( 'login_headerurl', 'ehutnik_login_logourl' );

function ehutnik_login_title() {
    return 'eHutník – přihlášení';
}
add_filter( 'login_headertitle', 'ehutnik_login_title' );

/**
 * Redirect on 404 page if rule exists in database (Actimmy edit)
*/
function ehutnik_redirect_oldweb() {
	if (is_404()) {
		$requestUrl = esc_url_raw($_SERVER['REQUEST_URI']);
		global $wpdb;
		$table_name = $wpdb->prefix . "redirect_rules";
		$query = "SELECT * FROM $table_name WHERE request_url = '$requestUrl'";
		$redirectRecord = $wpdb->get_row($query);

		if ((bool)$redirectRecord) {
			$redirectUrl = $redirectRecord->redirect_url;
			wp_redirect($redirectUrl, 301);
			exit();
		}
	}
}
add_action( 'template_redirect', 'ehutnik_redirect_oldweb' );

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}

	return $urls;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/library/vendors/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/library/vendors/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/library/vendors/template-functions.php';

/**
 * Custom functions for eHutnik.cz
 */
require get_template_directory() . '/library/vendors/ehutnik-functions.php';

/**
 * Advanced Ads customizations
 */
//require get_template_directory() . '/library/vendors/ehutnik-ads.php';

/**
 * Custom widgets for eHutnik.cz
 */
require get_template_directory() . '/library/vendors/ehutnik-widgets.php';

/**
 * inPocasi.cz weather data for eHutnik.cz
 */
require get_template_directory() . '/library/vendors/inmeteo-pocasi/pocasi-mesto.php';

/**
 * Customizer additions.
 */
//require get_template_directory() . '/library/vendors/customizer/customizer.php';

/**
 * Custom post types, fields and options for this theme
 */
require get_template_directory() . '/library/vendors/ehutnik-custom_posts_and_fields.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
    require get_template_directory() . '/library/vendors/jetpack.php';
}
