<?php
/**
 * Template Name: Příjem inzerce
 *
 * This is the template that formats page with advertisement forms.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

get_header(); ?>

	<!--<div id="primary" class="content-area">-->
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.

			// Printing forms
			// Check if the repeater field has rows of data
			if( have_rows('formular-inzerce') ):

				$counter = 1;
				echo '<div class="accordion inzerce"><dl>';
				// loop through the rows of data
				while ( have_rows('formular-inzerce') ) : the_row();

					// display a form
					echo '<dt><a href="#accordion'.$counter.'" aria-expanded="false" aria-controls="accordion'.$counter.'" class="accordion-title accordionTitle is-collapsed js-accordionTrigger"><i class="icon '.get_sub_field('inzerce_ikona').'"></i>'.get_sub_field('inzerce_nadpis_formulare').'<span class="icon caret"></span></a></dt>';
					echo '<dd class="accordion-content accordionItem is-collapsed" id="accordion'.$counter.'" aria-hidden="true">';
					echo '<h2>'.get_sub_field('inzerce_nadpis_formulare').'</h2>';
					echo do_shortcode('[contact-form-7 id="'.get_sub_field('inzerce_id_formulare').'" title="'.get_sub_field('inzerce_nadpis_formulare').'"]');
					echo '</dd>';
					$counter++;

				endwhile;
				echo '</dl></div>';

			else :

				// no rows found

			endif;

			?>

		</main><!-- #main -->
	<!--</div><!-- #primary -->

<?php
get_footer();
