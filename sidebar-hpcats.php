<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eHutnik_1.0
 */

if ( ! is_active_sidebar( 'sidebar-hpcats' ) ) {
	return;
}
?>

<!-- <aside id="secondary-content" class="secondary-content"> -->
	<?php dynamic_sidebar( 'sidebar-hpcats' ); ?>
<!-- </aside> --><!-- #secondary-content -->
