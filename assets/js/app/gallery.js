/**
 * Lightbox eHutnik
 * Initialize and control lightboxes for eHutnik's articles
 */

(function($) {

    $('.lightbox-bg').on('DOMMouseScroll mousewheel', function (e) {
        var up = false;
        if (e.originalEvent) {
            if (e.originalEvent.wheelDelta) up = e.originalEvent.wheelDelta / -1 < 0;
            if (e.originalEvent.deltaY) up = e.originalEvent.deltaY < 0;
            if (e.originalEvent.detail) up = e.originalEvent.detail < 0;
        }

        var prevent = function () {
            e.stopPropagation();
            e.preventDefault();
            e.returnValue = false;
            return false;
        }

        if (!up && this.scrollHeight <= $(this).innerHeight() + this.scrollTop + 1) {
            return prevent();
        } else if (up && 0 >= this.scrollTop - 1) {
            return prevent();
        }
    });

    var lightbox = $('.lightbox-bg'),
        lightboxGallery = $('.lightbox-bg-gallery'),
        lightboxSingle = $('.lightbox-bg-single'),
        closeButton = $('#gallery-close'),
        galleryThumbs = $('#gallery-thumbs'),
        galleryImages = $('#gallery'),
        thumb = galleryThumbs.find('.thumb');

    var minThumbs = 4;
    var maxThumbs = 5;

    var tabletWidth = 740;
    var screenWidth = $(window).width();

    function closeGallery() {
        var galleryHeader = $('.post-gallery-container .gallery-header');

        // Close the lightbox
        lightbox.css('display', 'none');

        // Remove article data from the gallery header
        galleryHeader.remove();
    }

    closeButton.click(function(e) {
        closeGallery()
    });

    lightbox.click(function(e) {
        if (e.target != this) {
            return false;
        }
        closeGallery()
    });

    $('#postGallery').click(function() {
        // Get article header data for the gallery
        var title = $('.entry-header .entry-title').html();
        var meta = $('.entry-header .entry-meta-row').html();
        var container = $('.post-gallery-container');

        // Get header of the article and prepend it to the gallery
        container.prepend('<div class="gallery-header"><h2>' + title + '</h2><div class="entry-meta-row">' + meta + '</div></div>');

        // show the lightbox
        lightboxGallery.css('display', 'block');

        if (screenWidth >= tabletWidth) {

            var gallerySlider = galleryImages.slick({
                //beforeChange: moveSlides,
                infinite: false,
                dots: false,
                prevArrow: '<button class="slick-prev slick-arrow slick-disabled" aria-label="Předchozí" type="button" aria-disabled="true" style="display: block;"><i>Předchozí</i></button>',
                nextArrow: '<button class="slick-next slick-arrow slick-disabled" aria-label="Next" type="button" aria-disabled="true" style="display: block;"><i>Další</i></button>',
                asNavFor: '#gallery-thumbs',
                lazyLoad: 'ondemand',
                mobileFirst: true
            });

            var thumbsSlider = galleryThumbs.slick({
                slidesToShow: maxThumbs,
                //slidesToScroll: maxThumbs,
                infinite: false,
                dots: false,
                vertical: true,
                prevArrow: '<button class="slick-prev slick-arrow slick-disabled" aria-label="Předchozí" type="button" aria-disabled="true" style="display: block;"><i>Předchozí</i></button>',
                nextArrow: '<button class="slick-next slick-arrow slick-disabled" aria-label="Next" type="button" aria-disabled="true" style="display: block;"><i>Další</i></button>',
                asNavFor: '#gallery',
                focusOnSelect: true,
                lazyLoad: 'ondemand',
                mobileFirst: true
            });
        }

    });
    $('#postImage-single').click(function() {
        // Get article header data for the gallery
        var title = $('.entry-header .entry-title').html();
        var meta = $('.entry-header .entry-meta-row').html();
        var container = $('.post-gallery-container');

        // Get header of the article and prepend it to the gallery
        container.prepend('<div class="gallery-header"><h2>' + title + '</h2><div class="entry-meta-row">' + meta + '</div></div>');

        // show the lightbox
        lightboxSingle.css('display', 'block');
    });

})(jQuery);
