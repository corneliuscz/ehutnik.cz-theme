/**
 * Extra validations for Advertisement forms
 */

(function($) {
    var timer = null;
    var vyskaInput = $('input[name=inzerat-vyska]');
    var modulo = null;
    var resObj = $('#validateResult');

    vyskaInput.on('keydown change click', function(){
       clearTimeout(timer);
       timer = setTimeout(doStuff, 1000)
   });

   function doStuff() {
       var vyska = vyskaInput.val();
       modulo = vyska % 4;

       if ( (modulo != 0) || (vyska == 0) ) {
           resObj.html('<span role="alert" class="wpcf7-not-valid-tip alert-inline"><i class="icon tick-er"></i> Výška inzerátu musí být dělitelná 4, opravte prosím rozměr</span>');
       } else {
           resObj.html('<span role="alert"><i class="icon tick-ok"></i></span>');
       }
   }
})(jQuery);

(function ($) {

    //hide all inputs except the first one
    $('label.hide_this_file').not(':eq(0)').hide();

    var hidden_files_count = $('label.hide_this:not(:visible)').length;

    //functionality for add-file link
    $('a.add_file').on('click', function (e) {
        //show by click the first one from hidden inputs
        $('label.hide_this_file:not(:visible):first').show(150);

        // hide Add file button if there are no options to add
        hidden_files_count = $('label.hide_this_file:not(:visible)').length;

        if (hidden_files_count == 0) {
            $('a.add_file').hide(150);
        }

        e.preventDefault();
    });

    //functionality for del-file link
    $('a.del_file').on('click', function (e) {
        //var init
        var input_parent = $(this).parent();
        var input_wrap = input_parent.find('span');

        //reset field value
        input_wrap.html(input_wrap.html());

        //hide by click
        input_parent.hide(150);

        // show button to add file
        hidden_files_count = $('label.hide_this_file:not(:visible)').length;
        if ((hidden_files_count + 1) > 0) {
            $('a.add_file').show(150);
        }

        e.preventDefault();
    });


})(jQuery);
