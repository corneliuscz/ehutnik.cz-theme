/* **************************** */
/*  Smooth scrolling to anchor  */
/* **************************** */

(function( $ ) {
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            var target = $(this.hash);

            // Change URL if possible
            if (history.pushState) {
                history.pushState(null, null, this.hash);
            }

            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 150
                }, 500);
            }
        }
    });

/* **************************** */
/*    And to another page :)    */
/* **************************** */

  	$( window ).load(function () {
      //window.scrollTo(0, 0);
      if (window.location.hash) {
        $('html,body').animate({
            scrollTop: $(window.location.hash).offset().top - 150
        }, 500);
      };
  	});

})( jQuery );
