/**
 * Script to manipulate Accordeons
 */

// Original  from https://codepen.io/chriswrightdesign/pen/cmanI
// Rewitter for use with jQuery

(function($) {
    var d = document,
        accordionToggles = $('.js-accordionTrigger'),
        setAria,
        setAccordionAria,
        switchAccordion,
        touchSupported = ('ontouchstart' in window),
        pointerSupported = ('pointerdown' in window);

    skipClickDelay = function(e) {
        e.preventDefault();
        e.target.click();
    }

    setAriaAttr = function(el, ariaType, newProperty) {
        el.attr(ariaType, newProperty);
    };
    setAccordionAria = function(el1, el2, expanded) {
        switch (expanded) {
            case "true":
                setAriaAttr(el1, 'aria-expanded', 'true');
                setAriaAttr(el2, 'aria-hidden', 'false');
                break;
            case "false":
                setAriaAttr(el1, 'aria-expanded', 'false');
                setAriaAttr(el2, 'aria-hidden', 'true');
                break;
            default:
                break;
        }
    };
    //function
    switchAccordion = function(e) {
        //console.log("triggered");
        e.preventDefault();
        var thisAnswer = $(this).parent().next();
        var thisQuestion = $(this);
        if (thisAnswer.hasClass('is-collapsed')) {
            setAccordionAria(thisQuestion, thisAnswer, 'true');
        } else {
            setAccordionAria(thisQuestion, thisAnswer, 'false');
        }
        thisQuestion.toggleClass('is-collapsed');
        thisQuestion.toggleClass('is-expanded');
        thisAnswer.toggleClass('is-collapsed');
        thisAnswer.toggleClass('is-expanded');

        thisAnswer.toggleClass('animateIn');
    };
    for (var i = 0, len = accordionToggles.length; i < len; i++) {
        if (touchSupported) {
            accordionToggles[i].addEventListener('touchstart', skipClickDelay, false);
        }
        if (pointerSupported) {
            accordionToggles[i].addEventListener('pointerdown', skipClickDelay, false);
        }
        accordionToggles[i].addEventListener('click', switchAccordion, false);
    }

})(jQuery);
