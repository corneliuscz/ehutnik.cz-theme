/*
 * Function that expands searchbox in the header
 * =============================================
 * Expanding is done in CSS – _menus.scss,
 * This script shows/hides submit button (delays blur() function that helps user to click the button)
 */

(function( $ ) {
  $('.header-searchbox input[type="search"]').on('focus', function() {
    $('.header-searchbox').addClass('focused');
  });

  $('.header-searchbox input[type="search"]').on('blur', function(e) {
    var obj = $(this);
            // Delay to allow click to execute
            setTimeout(function () {
                if (e.type == 'blur') {
                  $('.header-searchbox').removeClass('focused');
                }
            }, 250);
  });
})( jQuery );
