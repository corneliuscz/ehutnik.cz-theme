<?php
/**
 * The Homepage template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eHutnik_1.0
 */

get_header(); ?>

    <!--<div id="primary" class="content-area">-->
    <?php get_sidebar('topinfo') ?>
    <main id="main" class="site-main" role="main">

<?php
    /**
      * Get sticky posts
      *
      * If there is one then display it
      * Else use latest from News section
     */
    $news_of_the_day = 0;                            // Display "News of the day" label?
    $sticky_ID = FALSE;

    $stickyArgs = array(
        'posts_per_page' => 1,
        'post__in' => get_option('sticky_posts'),
        'post_status' => 'publish',
        'ignore_sticky_posts' => 1,
        'order' => 'DESC',
        'orderby' => 'post_date'
    );                                               // Get Sticky articles (published ones)
    $stickyQuery = new WP_Query( $stickyArgs );

    if ( $stickyQuery->have_posts() ) {
        while ( $stickyQuery->have_posts() ) {
            $stickyQuery->the_post();
            $sticky_ID = $post->ID;
        }
        wp_reset_postdata();
    }                                                // Get the ID of the last sticky article

    if ( isset( $sticky_ID ) ) {                     // Main article is the newest stickied one
        $main_article_ID = $sticky_ID;
    } else {                                         // Else get the newest from main category
        $newest = get_posts("cat=1&numberposts=1&fields=ids");
        $main_article_ID = $newest[0];
    }

    $post = get_post( $main_article_ID );            // get $post data for main article
    get_template_part( 'template-parts/main-post' ); // Display main article

?>
<?php
        $news_args = array (
                            'post__not_in' => array ( $main_article_ID ),
                            'posts_per_page' => 8,
                            'ignore_sticky_posts' => true,
                            'post_status' => 'publish',
                            'cat' => '-4',        // Příspěvky z kategorie 4 (Dobré zprávy) nechceme
                            // nechceme ani přípěvky ve formátu Status
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'post_format',
                                    'field' => 'slug',
                                    'terms' => array( 'post-format-status' ),
                                    'operator' => 'NOT IN'
                                )
                            )
                        );
        $news_query = new WP_Query( $news_args );
        if ( $news_query->have_posts() ) {

            $post_counter = 0;

            /* Start the Loop with main news */
            while ( $news_query->have_posts() ) {
                $news_query->the_post();

                /*
                 * Include the Post-Format-specific template for the content.
                 * If you want to override this in a child theme, then include a file
                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                 */
                get_template_part( 'template-parts/content', get_post_format() );
                $post_counter++;

                if ( ($news_query->found_posts < 4) && ($post_counter == 2) ) {
                    //if( function_exists('the_ad_placement') ) { the_ad_placement('pr-clanky'); }
                    ehutnik_showArticleAds();
                }
                if ( ($news_query->found_posts >= 4) && ($post_counter == 4) ) {
                    //if( function_exists('the_ad_placement') ) { the_ad_placement('pr-clanky'); }
                    ehutnik_showArticleAds();
                }

            }

        } else {

            get_template_part( 'template-parts/content', 'none' );

        }

        wp_reset_postdata();
?>
        <a href="<?php echo get_category_link( 1 ); ?>">Další zprávy z regionu</a>
    </main><!-- #main -->
    <!--</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
